﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMall.net.Models;
using EMallDataAccess.Models;
using Microsoft.AspNetCore.Http;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Views;
using System.Data.SqlClient;
using System.Net;
using System.Transactions;
using System.Net.Mail;

namespace EMall.net.Controllers
{
    public class ShoppingController : Controller
    {
        public ActionResult Index()
        {
            Shopping shopping = new Shopping();
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            if (BuyerId != null)
            {
                List<CartItemView> ciList = CartItemOperator.Single.FindByBuyer(BuyerId.Value);
                Dictionary<int, List<CartItemView>> dicCarts = new Dictionary<int, List<CartItemView>>();
                foreach (var item in ciList)
                {
                    if (!dicCarts.ContainsKey(item.CartItem.SellerID.Value))
                    {
                        dicCarts.Add(item.CartItem.SellerID.Value, new List<CartItemView>());
                    }
                    dicCarts[item.CartItem.SellerID.Value].Add(item);
                }
                shopping.allCartItems = dicCarts;
            }

            //shopping.allSellers = from ciList select ciList.
            return View(shopping);
        }
        
        public ActionResult ProductList(int? id)
        {

            ProductList pl = new ProductList();
            int totalCount;
            int skipCount = 0;
            int pageCount = 8;
            List<ProductCategory> cateList = ProductCategoryOperator.Single.FindTopCategories();
            List<MdlComProductCategory> mdlCategories = new List<MdlComProductCategory>();
            foreach (var item in cateList)
            {
                mdlCategories.Add(new MdlComProductCategory()
                {
                    ID = item.ID,
                    Name = item.Name,
                    ParentID = item.ParentID
                });
            }
            pl.AllProductCategories = mdlCategories;
            pl.CategoryID = id;
            if (id == null)
            {
                pl.AllProducts = ProductOperator.Single.FindAll(pageCount, 0);
                totalCount = ProductOperator.Single.FindAll().Count();
                
            }
            else
            {
                pl.AllProducts = ProductOperator.Single.FindByCategory(id.Value, pageCount, skipCount);
                totalCount = ProductOperator.Single.FindByCategory(id.Value).Count();

            }
            pl.PageNum = (totalCount + pageCount - 1) / pageCount;
            return View(pl);
        }
        [Route("/Shopping/ProductPagination/{cateId}/{pageNum}")]
        public ActionResult ProductPagination(int cateId, int pageNum)
        {
            int pageCount = 8;
            int skipCount = pageCount*(pageNum-1);
            List<ProductView> pvl;
            if (cateId == 0)
            {
                pvl = ProductOperator.Single.FindAll(pageCount, skipCount);
            } else
            {
                pvl = ProductOperator.Single.FindByCategory(cateId, pageCount, skipCount);
            }
            
            return PartialView("ProductPagination", pvl );
        }
        public ActionResult OrderConfirm()
        {
            OrderConfirm of = new OrderConfirm();
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            List<CartItemView> ciList = BuyerId == null ? null : CartItemOperator.Single.FindSelectedByBuyer(BuyerId.Value);
            Dictionary<int, List<CartItemView>> dicCarts = new Dictionary<int, List<CartItemView>>();
            foreach (var item in ciList)
            {
                if (!dicCarts.ContainsKey(item.CartItem.SellerID.Value))
                {
                    dicCarts.Add(item.CartItem.SellerID.Value, new List<CartItemView>());
                }
                dicCarts[item.CartItem.SellerID.Value].Add(item);
            }
            of.buyerId = BuyerId.Value;
            of.allCartItems = dicCarts;
            decimal totalBeforeTax = CartItemOperator.Single.FindSelectedSubtotalByBuyer(BuyerId.Value) + 15;
            decimal taxes = (totalBeforeTax + 15) * (decimal)0.15;
            of.totalBeforeTax = String.Format("{0:0.00}", totalBeforeTax);
            of.shippingBeforeTax = "15.00"; // 15*numberOfBuyers
            of.taxes = String.Format("{0:0.00}",taxes);
            of.totalWithShippingAndTaxes= String.Format("{0:0.00}", (totalBeforeTax + taxes));
            //shopping.allSellers = from ciList select ciList.
            return View(of);
        }
        [HttpPost]
        public IActionResult OrderConfirm(OrderConfirm of)
        {
            int orderID = 0;
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            List<CartItemView> ciList = BuyerId == null ? null : CartItemOperator.Single.FindSelectedByBuyer(BuyerId.Value);
            Dictionary<int, List<CartItemView>> dicCarts = new Dictionary<int, List<CartItemView>>();
            foreach (var item in ciList)
            {
                if (!dicCarts.ContainsKey(item.CartItem.SellerID.Value))
                {
                    dicCarts.Add(item.CartItem.SellerID.Value, new List<CartItemView>());
                }
                dicCarts[item.CartItem.SellerID.Value].Add(item);
            }
            of.buyerId = BuyerId.Value;
            of.allCartItems = dicCarts;
         
                try
                {
                    // 1. create summary record in 'orders' table for each seller
                    foreach (var cartPair in dicCarts)
                    {
                        var sellerId = cartPair.Value[0].CartItem.SellerID;
                        decimal totalBeforeTax = 0;
                        foreach (var ci in cartPair.Value)
                        {
                            totalBeforeTax += ci.UnitPrice * ci.CartItem.Quantity;
                        }

                        decimal shippingBeforeTax = 15;
                        decimal taxes = (totalBeforeTax + shippingBeforeTax) * (decimal)0.15;
                        decimal totalWithShippingAndTaxes = totalBeforeTax + shippingBeforeTax + taxes;
                        totalBeforeTax = decimal.Round(totalBeforeTax, 2, MidpointRounding.AwayFromZero);
                        taxes = decimal.Round(taxes, 2, MidpointRounding.AwayFromZero);
                        totalWithShippingAndTaxes = decimal.Round(totalWithShippingAndTaxes, 2, MidpointRounding.AwayFromZero);

                        Order order = new Order
                        {
                            BuyerID = of.buyerId,
                            Email = of.Email,
                            OrderTime = DateTime.Now,
                            OrderStatus = 0,
                            PhoneNo = of.PhoneNo,
                            RecieverName = of.Name,
                            ShippingAddress = String.Format("{0},{1},{2}", of.Address, of.City, of.Province),
                            Taxes = taxes,
                            Postalcode = of.PostalCode,
                            SellerID = sellerId.Value,
                            ShippingBeforeTax = shippingBeforeTax,
                            TotalBeforeTax = totalBeforeTax,
                            Total = totalWithShippingAndTaxes

                        };
                        OrderOperator.Single.Add(order);
                        orderID = order.ID;
                        // 2. copy all records from cartitems to 'orderitems' (select & insert)
                        
                            foreach (var ci in cartPair.Value)
                            {
                                OrderItem oi = new OrderItem
                                {
                                    BuyCount = ci.CartItem.Quantity,
                                    OrderID = orderID,
                                    ProductID = ci.CartItem.ProductID,
                                    BuyPrice = ci.UnitPrice,// * (1- discount),
                                    UnitPrice = ci.UnitPrice,
                                    Subtotal = ci.UnitPrice * ci.CartItem.Quantity

                                };
                                OrderOperator.Single.AddOrderItem(oi);
                                CartItemOperator.Single.Remove(ci.CartItem);
                            }
                        
                    } // end foreach (var cartPair in of.allCartItems) -- each seller
                      
                 
                } catch(Exception)
                {
                   
                }
          
            //return RedirectToAction("Pay", new Microsoft.AspNetCore.Routing.RouteValueDictionary( new { controller = "Shopping", action = "Pay", Id = orderID }));
            //return RedirectToAction("Pay", new { controller = "Shopping", action = "Pay", id = orderID });
            return RedirectToAction("OrderDetail", new { controller = "Shopping", action = "OrderDetail", id = orderID });
        }
        public ActionResult Pay(int id)
        {
            Order order = OrderOperator.Single.Find(id);

            return View(order);
        }
        [HttpPost]
        public IActionResult OrderPaid(int id)
        {
            Order order = OrderOperator.Single.Find(id);
            order.OrderStatus = OrderStatus.Paid;
            OrderOperator.Single.Update(order);
            // send confirm email
            String buyerEmail = HttpContext.Session.GetString("Email");
            String buyerName = HttpContext.Session.GetString("Name");
            SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

            smtpClient.Credentials = new System.Net.NetworkCredential("E-Mall-ipd20@hotmail.com", "aaAA123$");
            // smtpClient.UseDefaultCredentials = true; // uncomment if you don't want to use the network credentials
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.Subject = "Your E-Mall order #"+ id + " has been paid.";
            string htmlBody;
            mail.IsBodyHtml = true;
            htmlBody = String.Format("Hi {0}, <br><br>"+
                        "Thank you for purchase.  <br><br>"+
                        "Your order # {1} amount $ {2} has been received. <br><br>"+
                        "Item(s) you ordered will be shipped in 1 - 3 days.<br><br>"+
                        "Kind regards,<br><br>"+
                        "E - Mall", buyerName, id, order.Total );
            mail.Body = htmlBody;
            //Setting From , To and CC
            mail.From = new MailAddress("E-Mall-ipd20@hotmail.com", "E-Mall");
            mail.To.Add(new MailAddress(buyerEmail));
            mail.CC.Add(new MailAddress("E-Mall-ipd20@hotmail.com"));

            smtpClient.Send(mail);
            // end send confirm email
            return RedirectToAction("OrderDetail", new { controller = "Shopping", action = "OrderDetail", id = id });
        }

        public ActionResult OrderDetail(int id)
        {
            OrderView item = OrderOperator.Single.FindOrderView(id);
            MdlBuyerOrder mdl = new MdlBuyerOrder() {
              
                ID = item.Order.ID,
                BuyerID = item.Order.BuyerID,
                BuyerName = item.BuyerName,
                Email = item.Order.Email,
                OrderStatus = item.Order.OrderStatus,
                OrderTime = item.Order.OrderTime,
                PhoneNo = item.Order.PhoneNo,
                Postalcode = item.Order.Postalcode,
                RecieverName = item.Order.RecieverName,
                SellerID = item.Order.SellerID,
                SellerName = item.SellerName,
                ShippingAddress = item.Order.ShippingAddress,
                ShippingBeforeTax = item.Order.ShippingBeforeTax,
                Taxes = item.Order.Taxes,
                Total = item.Order.Total,
                TotalBeforeTax = item.Order.TotalBeforeTax
            };

            return View(mdl);
        }

        public ActionResult OrderList()
        {
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            OrderList orderList = new OrderList();
            if (BuyerId != null)
            {
                orderList.OrderListByBuyer = OrderOperator.Single.FindAllByBuyer(BuyerId.Value);
            } else
            {
                orderList.OrderListByBuyer = OrderOperator.Single.FindAllByBuyer(0);
            }

            return View(orderList);
        }

        [HttpPost]
        public async Task<IActionResult> Review(int productId, String review, int rating, int orderId,  int orderItemId)
        {
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            if (BuyerId!= null)
            {
                ProductReview productReview = new ProductReview {
                    BuyerID = BuyerId.Value,
                    CreationTime = DateTime.Now,
                    OrderID = orderId,
                    Review = review,
                    Rating = rating,
                    ProductID = productId
                };
                Product product = ProductOperator.Single.Find(productId);
                using (var transaction = EMallDbContext.Single.Database.BeginTransaction())
                {
                    try
                    {
                        // 1. insert product review record
                        ProductReviewOperator.Single.Add(productReview);
                        // 2. increase product review count, recalculate product rating
                        Decimal ratingAvg = ((product.Rating * product.ReviewCount) + rating) / (product.ReviewCount + 1);
                        product.Rating = Decimal.Round(ratingAvg, 2);
                        product.ReviewCount += 1;
                        
                        ProductOperator.Single.Update(product);
                        // 3. mark order item as reviewed
                        OrderItem orderItem = OrderOperator.Single.FindOrderItem(orderItemId);
                        orderItem.Reviewed = 1;
                        OrderOperator.Single.UpdateOrderItem(orderItem);
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return Json(new { success = false, message = "Review post failed" });
                    }
                }
            }

            return Json(new { success = true, message = "Review posted successfully" });

        }
        [HttpPost]
        public ActionResult ProductListAjax(int categoryID, int pageCount, int skipCount)
        {
            List<ProductView> ProductList = ProductOperator.Single.FindByCategory(categoryID, pageCount, skipCount);
            return Json(new { data = ProductList });
        }

        List<CartItem> cartItems = new List<CartItem>();
        //#region API Calls
        [HttpGet]
        public async Task<int> CartItemQty()
        {
            int qty = 0;
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);
                if (BuyerId == null)
                {
                    qty = 0;
                }
                else
                {
                    qty = CartItemOperator.Single.FindQtyByBuyer(BuyerId.Value);
                }
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return 0;
            }
            return qty;
        }
        [HttpGet]
        public async Task<String> CartItemSubtotal()
        {
            String subtotal = "0.00";
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);
                if (BuyerId == null)
                {
                    subtotal = "0.00";
                }
                else
                {
                    //await Task.Delay(TimeSpan.FromSeconds(0.1));
                    subtotal = CartItemOperator.Single.FindSubtotalByBuyer(BuyerId.Value);
                }
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return "0.00";
            }
            return subtotal;
        }
        [HttpPost]
        public async Task<IActionResult> AddToCart(int id, int quant)
        {
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            CartItemView ciDB = new CartItemView();
            Product pr = ProductOperator.Single.Find(id);

            CartItem newCi = new CartItem { ProductID = id, Quantity = quant, SellerID = pr.SellerID };

            newCi.BuyerID = BuyerId;
            newCi.CreationTS = DateTime.Now;

            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);
                CartItemOperator.Single.Add(BuyerId.Value, id, quant);
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return NotFound();
            }

            return Json(new { success = true, message = "Added successfully" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCart(int id, int quantity)
        {
            CartItemView ci = new CartItemView();
            Decimal sub = 0;
            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);
                //await Task.Delay(TimeSpan.FromSeconds(1));
                Decimal qty = Convert.ToDecimal(quantity);
                if (qty != 0)
                {
                    ci = CartItemOperator.Single.Find(id);
                    sub = Decimal.Round(ci.UnitPrice * quantity, 2);
                }
                CartItemOperator.Single.UpdateQty(id, qty);
                
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return NotFound();
            }
            
            //return Json(new { sub = ci.UnitPrice * quantity 
            return Json(sub);
        }

        [HttpPost]
        public async Task<IActionResult> Status(int id, int status)
        {
            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);

                CartItemOperator.Single.Update(id, status);
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return NotFound();
            }
            return Json(new { success = true, message = "Updated successfully" });
        }

        [HttpPost]
        public async Task<IActionResult> Statusstore(int id, int status)
        {
            int? BuyerId = HttpContext.Session.GetInt32("UserId");

            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);

                CartItemOperator.Single.UpdateStore(id, BuyerId.Value, status);
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return NotFound();
            }
            return Json(new { success = true, message = "Updated successfully" });
        }
        [HttpPost]
        public async Task<IActionResult> Statusall(int status)
        {
            int? BuyerId = HttpContext.Session.GetInt32("UserId");

            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);

                CartItemOperator.Single.UpdateAll(BuyerId.Value, status);
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            }
            catch (SqlException ex)
            {
                return NotFound();
            }
            return Json(new { success = true, message = "Updated successfully" });
        }
    }
}