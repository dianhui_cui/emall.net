﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMall.net.Models;
using EMallDataAccess.Models;
using Microsoft.AspNetCore.Http;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Views;
using System.Data.SqlClient;
using System.Net;
//using System.Web.Http;

namespace EMall.net.Controllers
{
    public class HomeController : Controller
    {
        /*public IActionResult Index()
        {
            return View();
        }*/
        public ActionResult Index()
        {

            Index idx = new Index();
            idx.allProductCategories = GetAllCategories();
            idx.allProducts = GetAllProducts();
            return View(idx);
        }
        //#region API Calls
        //[HttpGet]
        public List<MdlComProductCategory> GetAllCategories()
        {
            // return Json(new { data = await _db.Books.ToListAsync() });
            /*List<ProductCategory> VegSubCate = new List<ProductCategory>{
                new ProductCategory{ID=111, Name="Ordinary Vegetables", ParentID=11 },
                new ProductCategory{ID=112, Name="Organic Vegetables", ParentID=11}
            };
            List<ProductCategory> FruitSubCate = new List<ProductCategory>{
                new ProductCategory{ID=121, Name="Ordinary Fruits", ParentID=12 },
                new ProductCategory{ID=122, Name="Organic Fruits", ParentID=12}
            };
            List<ProductCategory> foodSubCate = new List<ProductCategory>{
                new ProductCategory{ID=11, Name="Vegetables", ParentID=1, ChildProductCategories=VegSubCate },
                new ProductCategory{ID=12, Name="Fruits", ParentID=1, ChildProductCategories=FruitSubCate }
            };
            return new List<ProductCategory>{
                new ProductCategory{ID=1, Name="Food", ChildProductCategories= foodSubCate},
                new ProductCategory{ID=2, Name="Clothing"}
                */
            List<ProductCategory> cateList = ProductCategoryOperator.Single.FindTopCategories();
            List<MdlComProductCategory> mdlCategories = new List<MdlComProductCategory>();
            foreach (var item in cateList)
            {
                mdlCategories.Add(new MdlComProductCategory() { 
                    ID=item.ID,
                    Name=item.Name,
                    ParentID=item.ParentID
                });
            }
            return mdlCategories;
        
            // Get top level categories which has ParentID=0
            // select * from cates where parentId=0
            // put subcategories of each root category into a list
            // ChildProductCategories
            // foreach(cate in rootCates) {
            // cate.subCateList = select *  from cates where parentId=cate.id;
            //  foreach(subCate in cate.subCateList){
            //       subCate.subCateList = select * from cates where parentId = subCate.id;
            //  }
            // }

        }
        //[HttpGet]
        public List<ProductView> GetAllProducts()
        {
            // return Json(new { data = await _db.Books.ToListAsync() });
            /*return new List<Product>{
                new Product{ID=1, Name="Apple Gala", PictureUrl="./img/AppleGala.jpg", SellerID=1},
                new Product{ID=2, Name="Banana", PictureUrl="./img/Banana.jpg",SellerID=1}

            };*/
            List<ProductView> productList = ProductOperator.Single.FindAll(4,0);
            return productList;
        }
        List<CartItem> cartItems = new List<CartItem>();
        #region API Calls
        
        [HttpPost]
        public async Task<IActionResult> AddToCart(int id, int quant )
        {
            // instantiate ci using information from products table. Buyer information get from session
            // HttpContext.Session.GetString("UserName");
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            CartItemView ciDB = new CartItemView();
            /*if (BuyerId != null)
            {
                // should be find by buyer and product
                ciDB = CartItemOperator.Single.FindByBuyer(BuyerId.Value).FirstOrDefault();
            }
            if (ciDB != null)
            {
                if (ciDB.CartItem.ProductID == id)
                {
                    ciDB.CartItem.Quantity += quant;
                    try
                    {
                        EMallDbContext.Single.CartItems.Update(ciDB.CartItem);
                        // EMallDbContext.Single.SaveChanges();
                        await EMallDbContext.Single.SaveChangesAsync();
                        return Json(new { success = true, message = "Added successfully" });
                        //cartItems.Add(ci);
                    }
                    catch (SqlException ex)
                    {
                        throw new System.Web.Http.HttpResponseException(HttpStatusCode.NotFound);
                    }
                }
            }*/
            Product pr = ProductOperator.Single.Find(id);
            
            CartItem newCi = new CartItem { ProductID = id, Quantity = quant, SellerID = pr.SellerID };
            
            
            newCi.BuyerID = BuyerId;
            newCi.CreationTS = DateTime.Now;
            
            //ci.SellerID
            //CartItem ci = new CartItem { ProductID = id, Quantity = model.quantity, BuyerID = BuyerId, CreationTS = DateTime.Now };
            //if (ModelState.IsValid)
            //{
            //CartItemOperator.Single.Add(ci);
            try
            {
                //EMallDbContext.Single.CartItems.Add(newCi);
                CartItemOperator.Single.Add(BuyerId.Value, id, quant);
                // EMallDbContext.Single.SaveChanges();
                //await EMallDbContext.Single.SaveChangesAsync();
                //cartItems.Add(ci);
            } catch(SqlException ex)
            {
                return NotFound();
            }
            //_db.SaveChanges();
            // return ci;
            //return RedirectToAction("BuyerLogin");
            //}
            //            return RedirectToAction("Index","Home");

            return Json(new { success = true, message = "Added successfully" });
        }
        /*public void AddToCart(int id, Index model)
        {
            // instantiate ci using information from products table. Buyer information get from session
            // HttpContext.Session.GetString("UserName");
            int? BuyerId = HttpContext.Session.GetInt32("UserId");
            CartItem ci = new CartItem { ProductID = id, Quantity = model.quantity, BuyerID = BuyerId, CreationTS = DateTime.Now };
            //if (ModelState.IsValid)
            //{
                CartItemOperator.Single.Add(ci);
            //}
            //return RedirectToAction("Index","Home");
            
            
        }*/
        /*[HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Json(new { data = await _db.Books.ToListAsync() });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var bookFromDb = await _db.Books.FirstOrDefaultAsync(u => u.Id == id);
            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            _db.Books.Remove(bookFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Delete successful" });
        }*/
        #endregion
        
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
