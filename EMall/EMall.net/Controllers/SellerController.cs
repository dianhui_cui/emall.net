﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EMall.net.Models;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using EMallDataAccess.Views;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EMall.net.Controllers
{
    public class SellerController : Controller
    {
        private readonly IHostingEnvironment hostingEnvironment;
        public SellerController(IHostingEnvironment environment)
        {
            hostingEnvironment = environment;
        }
        private MdlSellerSession sellerSession;
        public IActionResult Index()
        {

            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            MdlSellerIndex mdlIndex = new MdlSellerIndex()
            {
                Seller = seller,
                StatisticViews = seller == null ? null : OrderOperator.Single.FindStatisticViews(seller.ID)
            };
            ViewData.Model = mdlIndex;
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(MdlSellerLogin mdl)
        {
            if (ModelState.IsValid)
            {
                EMallDataAccess.Models.User seller = EMallDataAccess.DataAccess.UserOperator.Single.Login(mdl.Username, mdl.Password, EMallDataAccess.Models.UserType.Seller);
                if (null == seller)
                {
                    ViewBag.Username = mdl.Username;
                    return View();
                }
                else
                {
                    MdlSellerSession sellerModel = new MdlSellerSession()
                    {
                        ID = seller.ID,
                        Username = seller.Username,
                        FullName = string.IsNullOrEmpty(seller.FirstName) ? seller.Username : string.Format("{0} {1}", seller.FirstName, seller.LastName)
                    };
                    HttpContext.Session.SetString("Seller", JsonConvert.SerializeObject(sellerModel));
                    return RedirectToAction("Index", "Seller");
                    //return Index();
                }
            }
            else
            {
                ViewBag.Username = mdl.Username;
                return View();
            }
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Register(SellerRegistrationModel sellerRegistration)
        {
            if (ModelState.IsValid)
            {
                EMallDataAccess.Models.User seller = new EMallDataAccess.Models.User()
                {
                    Username = sellerRegistration.Username,
                    Address = sellerRegistration.Address,
                    City = sellerRegistration.City,
                    Country = sellerRegistration.Country,
                    Description = sellerRegistration.Description,
                    Email = sellerRegistration.Email,
                    FirstName = sellerRegistration.FirstName,
                    MiddleName = sellerRegistration.MiddleName,
                    LastName = sellerRegistration.LastName,
                    OrderCount = 0,
                    Password = sellerRegistration.Password,
                    PhoneNo = sellerRegistration.PhoneNo,
                    PostalCode = sellerRegistration.PostalCode,
                    Province = sellerRegistration.Province,
                    RegisterTime = DateTime.Now,
                    Score = 0,
                    ShopPage = string.Empty,
                    UserType = EMallDataAccess.Models.UserType.Seller
                };
                UserOperator.Single.Add(seller);

                MdlSellerSession sellerModel = new MdlSellerSession()
                {
                    ID = seller.ID,
                    Username = seller.Username,
                    FullName = string.IsNullOrEmpty(seller.FirstName) ? seller.Username : string.Format("{0} {1}", seller.FirstName, seller.LastName)
                };
                HttpContext.Session.SetString("Seller", JsonConvert.SerializeObject(sellerModel));
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                ViewBag.Username = sellerRegistration.Username;
                ViewBag.Email = sellerRegistration.Email;
                ViewBag.FirstName = sellerRegistration.FirstName;
                ViewBag.MiddleName = sellerRegistration.MiddleName;
                ViewBag.LastName = sellerRegistration.LastName;
                ViewBag.Address = sellerRegistration.Address;
                ViewBag.City = sellerRegistration.City;
                ViewBag.Province = sellerRegistration.Province;
                ViewBag.Country = sellerRegistration.Country;
                ViewBag.PostalCode = sellerRegistration.PostalCode;
                ViewBag.PhoneNo = sellerRegistration.PhoneNo;
                ViewBag.Description = sellerRegistration.Description;
                return View();

            }
        }
        public IActionResult PersonalSetting()
        {
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                EMallDataAccess.Models.User user = UserOperator.Single.Find(seller.ID);
                UserBasicInfo basicInfo = new UserBasicInfo()
                {
                    ID = user.ID,
                    Email = user.Email,
                    Username = user.Username,
                    FirstName = user.FirstName,
                    MiddleName = user.MiddleName,
                    LastName = user.LastName,
                    PhoneNo = user.PhoneNo
                };
                UserAddressInfo addressInfo = new UserAddressInfo()
                {
                    ID = user.ID,
                    Address = user.Address,
                    City = user.City,
                    Province = user.Province,
                    Country = user.Country,
                    PostalCode = user.PostalCode
                };
                UserDescriptionInfo descriptionInfo = new UserDescriptionInfo()
                {
                    ID = user.ID,
                    Description = user.Description
                };
                UserChangePasswordInfo passwordInfo = new UserChangePasswordInfo();
                MdlSellerPersonal mdlSellerPersonal = new MdlSellerPersonal()
                {
                    Seller = seller,
                    UserBasicInfo = basicInfo,
                    UserAddressInfo = addressInfo,
                    UserDescriptionInfo = descriptionInfo,
                    UserChangePasswordInfo = passwordInfo
                };
                ViewData.Model = mdlSellerPersonal;
                return View();
            }
        }

        public IActionResult Product()
        {
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                Dictionary<int, CategoryProducts> dicProducts = new Dictionary<int, CategoryProducts>();
                List<ProductView> products = ProductOperator.Single.FindSeller(seller.ID);
                foreach (var item in products)
                {
                    if (!dicProducts.ContainsKey(item.Product.CategoryID))
                    {
                        dicProducts.Add(item.Product.CategoryID, new CategoryProducts() { CategoryID = item.Product.CategoryID, CategoryName = item.CategoryName, Products = new List<ProductView>() });
                    }
                    dicProducts[item.Product.CategoryID].Products.Add(item);
                }

                MdlSellerProducts mdlProducts = new MdlSellerProducts()
                {
                    Seller = seller,
                    DicProducts = dicProducts
                };
                ViewData.Model = mdlProducts;
                return View();
            }
        }
        [HttpGet]
        public IActionResult AddProduct()
        {
            int id = int.Parse( HttpContext.Request.Query["id"]);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                List<ProductCategory> cateList = ProductCategoryOperator.Single.FindAll();
                List<MdlComProductCategory> mdlCategories = new List<MdlComProductCategory>();
                foreach (var item in cateList)
                {
                    mdlCategories.Add(new MdlComProductCategory()
                    {
                        ID = item.ID,
                        Name = item.Name,
                        ParentID = item.ParentID
                    });
                }
                MdlSellerProductAddUpdate mdlProduct = new MdlSellerProductAddUpdate() { Seller = seller, Categories = mdlCategories };
                if (0 == id)
                {
                    mdlProduct.ID = 0;
                }
                else
                {
                    Product product = ProductOperator.Single.Find(id);
                    mdlProduct.ID = id;
                    mdlProduct.SellerID = product.SellerID;
                    mdlProduct.CategoryID = product.CategoryID;
                    mdlProduct.Description = product.Description;
                    mdlProduct.Discount = product.Discount;
                    mdlProduct.Inventory = product.Inventory;
                    mdlProduct.Name = product.Name;
                    mdlProduct.PictureUrl = product.PictureUrl;
                    mdlProduct.Rating = product.Rating;
                    mdlProduct.ReviewCount = product.ReviewCount;
                    mdlProduct.SalesCount = product.SalesCount;
                    mdlProduct.Unit = product.Unit;
                    mdlProduct.UnitPrice = product.UnitPrice;
                }
                ViewData.Model = mdlProduct;
                return View();
            }
        }
        [HttpPost]
        public IActionResult AddProduct(MdlSellerProductAddUpdate mdlProduct)
        {
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (mdlProduct.ID == 0)
                    {
                        Product product = new Product()
                        {
                            ID=0,
                            SellerID = seller.ID,
                            CategoryID = mdlProduct.CategoryID,
                            Description = mdlProduct.Description==null?"" : mdlProduct.Description,
                            Discount = mdlProduct.Discount,
                            Inventory = mdlProduct.Inventory,
                            Name = mdlProduct.Name,
                            Rating = 0,
                            ReviewCount = 0,
                            SalesCount = 0,
                            Unit = mdlProduct.Unit,
                            UnitPrice = mdlProduct.UnitPrice
                        };
                        if (mdlProduct.Picture != null)
                        {
                            var uniqueFileName = GetUniqueFileName(mdlProduct.Picture.FileName);
                            var uploads = Path.Combine(hostingEnvironment.WebRootPath, "uploads");
                            var filePath = Path.Combine(uploads, uniqueFileName);
                            mdlProduct.Picture.CopyTo(new FileStream(filePath, FileMode.Create));
                            product.PictureUrl = "~/uploads/" + uniqueFileName;
                        }
                        ProductOperator.Single.Add(product);
                    }
                    else
                    {
                        Product product = ProductOperator.Single.Find(mdlProduct.ID);
                        product.Description = mdlProduct.Description==null?"":mdlProduct.Description;
                        product.Discount = mdlProduct.Discount;
                        product.Inventory = mdlProduct.Inventory;
                        product.Name = mdlProduct.Name;

                        product.Unit = mdlProduct.Unit;
                        product.UnitPrice = mdlProduct.UnitPrice;
                        if (mdlProduct.Picture != null)
                        {
                            var uniqueFileName = GetUniqueFileName(mdlProduct.Picture.FileName);
                            var uploads = Path.Combine(hostingEnvironment.WebRootPath, "uploads");
                            var filePath = Path.Combine(uploads, uniqueFileName);
                            mdlProduct.Picture.CopyTo(new FileStream(filePath, FileMode.Create));
                            product.PictureUrl = "~/uploads/" + uniqueFileName;
                        }
                        ProductOperator.Single.Update(product);
                    }
                    return RedirectToAction("Product", "Seller");
                }
                else
                {
                    return View(mdlProduct);
                }
            }


        }
        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        [HttpGet]
        public IActionResult DeleteProduct()
        {
            int id = int.Parse(HttpContext.Request.Query["id"]);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                List<string> errors = new List<string>();
                Product product = ProductOperator.Single.Find(id);
                if (null == product)
                {
                    errors.Add("Product not found");
                }
                else
                {
                    if (product.SellerID != seller.ID)
                    {
                        errors.Add("Operation Forbidden, Delete product  failed.");
                    }
                }
                MdlSellerProductDelete productDelete = new MdlSellerProductDelete() {
                    Seller = seller,
                    Errors = errors,
                    ID = product == null ? 0 : product.ID,
                    Name = product == null ? "" : product.Name,
                    PictureUrl = product == null ? "" : product.PictureUrl


                };
                ViewData.Model = productDelete;
                return View();


            }

        }
        [HttpPost]
        public IActionResult DeleteProduct(MdlSellerProductDelete productDelete)
        {
            ProductOperator.Single.Delete(productDelete.ID);

            return RedirectToAction("Product", "Seller");
        }

        [HttpGet]
        public IActionResult SalePerformance()
        {
            string period = HttpContext.Request.Query["period"];
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                MdlSellerSalePerformance mdlSeller = new MdlSellerSalePerformance() { Seller = seller,Period=period };
                ViewData.Model = mdlSeller;
                return View();
            }

        }

        [HttpGet]
        public IActionResult OrderInProcess()
        {
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                MdlSellerOrderInProcess mdl = new MdlSellerOrderInProcess() { Seller=seller};
                ViewData.Model = mdl;
                return View();
            }

        }
        [HttpGet]
        public IActionResult OrderHistory()
        {
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null == seller)
            {
                return RedirectToAction("Index", "Seller");
            }
            else
            {
                MdlSellerOrderHistory mdl = new MdlSellerOrderHistory() { Seller = seller };
                ViewData.Model = mdl;
                return View();
            }

        }
    }
}