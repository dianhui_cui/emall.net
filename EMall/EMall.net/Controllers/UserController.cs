﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EMall.net.Models;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EMall.net.Controllers
{
    public class UserController : Controller
    {
        [BindProperty]
        public new EMall.net.Models.Buyer User { get; set; }
        public ActionResult BuyerRegister()
        {
            User = new EMall.net.Models.Buyer();
            
            return View(User);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyerRegisterPost()
        {
            

                //_db.Users.Add(User);

                //_db.SaveChanges();
                EMallDataAccess.Models.User UserDB = new EMallDataAccess.Models.User{ Username = User.Username, Email = User.Email, Password = User.Password, UserType=UserType.Buyer, RegisterTime = DateTime.Now };
                UserOperator.Single.Add(UserDB);
                return RedirectToAction("BuyerLogin");
            
        }
        public ActionResult BuyerLogin()
        {
            User = new EMall.net.Models.Buyer();

            return View(User);
        }

        public ActionResult BuyerLoginPost()
        {
            //if (ModelState.IsValid)
            //{
            // UserDB = _db.Users.FirstOrDefault(u => u.Username == Username);
            // 
            EMallDataAccess.Models.User UserDB = UserOperator.Single.Login(User.Username, User.Password, UserType.Buyer);
                if (UserDB == null)
                {
                    return RedirectToAction("BuyerLogin");
                }
                
                /*if (User.Password != UserDB.Password)
                {
                    return RedirectToAction("BuyerLogin");
                }*/
                
            HttpContext.Session.SetString("UserName",User.Username);
            HttpContext.Session.SetInt32("UserId", UserDB.ID);
            HttpContext.Session.SetString("Name", UserDB.FirstName+" " +UserDB.LastName);
            HttpContext.Session.SetString("Address", UserDB.Address);
            HttpContext.Session.SetString("City", UserDB.City);
            HttpContext.Session.SetString("Province", UserDB.Province);
            HttpContext.Session.SetString("PostalCode", UserDB.PostalCode);
            HttpContext.Session.SetString("Email", UserDB.Email);
            HttpContext.Session.SetString("PhoneNo", UserDB.PhoneNo);
            return RedirectToAction("Index","Home");
            //}
            //return RedirectToAction("/Home/Index");
        }

        public ActionResult BuyerLogOut()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        /*[HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var bookFromDb = await _db.Books.FirstOrDefaultAsync(u => u.Id == id);
            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            _db.Books.Remove(bookFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Delete successful" });
        }*/
        //#endregion

    }
}