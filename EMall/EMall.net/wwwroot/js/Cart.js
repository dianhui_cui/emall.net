﻿function increment(id) {
    var value = $("input[name=quantity" + id + "]").val();
    value++;
    $("input[name=quantity" + id + "]").val(value);
    $("#update" + id).show();
}
function decrement(id) {
    var value = $("input[name=quantity" + id + "]").val();
    if (value > 0) {
        value--;
    }
    $("input[name=quantity" + id + "]").val(value);
    $("#update" + id).show();
}
function update(e, id) {
    e.preventDefault();
    var quantity = $("input[name=quantity" + id + "]").val();
    console.log("quantity is " + quantity);
    $.ajax({ // FIXME: escape special characters using urlencode  "/Shopping/UpdateCart/",
        url: '/Shopping/UpdateCart',
        type: "POST",
        data: {
            id: id,
            quantity: quantity
        },
        error: function (jqxhr, status, errorThrown) {
            toastr.error("Update failed");
        }
    }).done(function (data) {
        $("#update" + id).hide();
        console.log(data);
        //var subtotal = jQuery.parseJSON(data);
        $("#subtotal"+id).html("$ " + data);
        toastr.success("Update succeeded");
    });

    /*$.get("/cart/update/" + id + "/" + quantity, function() {
        console.log("quantity updated");
        $("#update" + id).hide();
    });*/
    if (quantity == 0) {
        $("#itemrow" + id).hide();
    }
}

function toggleCheck(e, id) {
    //e.preventDefault();
    console.log("id is " + id);
    var status = $("input[name=chkCartItem" + id + "]").prop("checked") ? 1 : 0;
   /* var inputVal = $("input[name=chkCartItem" + id + "]").val();
    var inputSts = $("input[name=chkCartItem" + id + "]").prop("checked");
    console.log("input value is " + inputVal);
    console.log("input sts is " + inputSts);*/

    // console.log($("input[name=chkCartItem" + id + "]").prop("checked"));
    // alert("status is " + $("input[name=chkCartItem" + id + "]").attr('name'));
    status = parseInt(status);
    console.log("status is " + status);
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/Status/",
        type: "POST",
        data: {
            id: id,
            status: status
        },
        error: function (jqxhr, status, errorThrown) {
            toastr.error(jqxhr.responseText);
        },
        success:function() {
            
            toastr.success("select/unselect succeeded");
        }
    });

}

function toggleCheckSeller(e, id) {
    //e.preventDefault();
    var status = $("input[name=chkSeller" + id + "]").prop("checked") ? 1 : 0;
    status = parseInt(status);
    console.log("sts is " + status);
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/Statusstore/",
        type: "POST",
        data: {
            id: id,
            status: status
        },
        error: function (jqxhr, status, errorThrown) {
            toastr.error(jqxhr.responseText);
        },
        success: function () {

            toastr.success("select/unselect succeeded");
        }
    });
    var table = $("#tableSeller" + id);
    // var table= $(e.target).closest('table');
    // alert(table.attr('id'));
    $('td input[type=checkbox]', table).prop('checked', $("input[name=chkSeller" + id + "]").prop("checked"));
    /*if (status==1) {
        console.log("table id is " + $('#tableSeller3').attr('id'));
        alert("name is " + $("#tableSeller3 > tr > td > input[type='checkbox']:eq(1)").attr('name'));
        alert("name is " + $("#tableSeller" + id + " input[type=checkbox]:eq(1)").attr('name'));
            $("#tableSeller" + id + " input[type=checkbox]").prop('checked', true);
        } else {
            $("#tableSeller" + id + " input[type=checkbox]").prop('checked', false);
        }*/
}

$(document).ready(function () {
    //getItemCount();

    $(".updateLink").hide();
    $('.cartQuantity').bind('propertychange change input paste', function () {
        var id = $(this).attr('cartId');
        $("#update" + id).show();
    });
    $("input[name=chkSelectAll]").change(function () {
        if (this.checked) {
            $("input[type=checkbox]").prop('checked', true);
        } else {
            $("input[type=checkbox]").prop('checked', false);
        }
        var status = this.checked ? 1 : 0;
        status = parseInt(status);
        $.ajax({ // FIXME: escape special characters using urlencode
            url: "/Shopping/Statusall/",
            type: "POST",
            data: {status:status},
            error: function (jqxhr, status, errorThrown) {
                toastr.error(jqxhr.responseText);
            },
            success: function () {

                toastr.success("select/unselect succeeded");
            }
        });
    });
});