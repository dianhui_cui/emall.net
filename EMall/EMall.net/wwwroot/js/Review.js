﻿
        var ratedIndex = -1, uID = 0;
        var rating = 0;
        var productId = 0;
        var orderId = 0;
        var orderitemId = 0;
        $(document).ready(function () {
            resetStarColors();
            /*
            if (localStorage.getItem('ratedIndex') != null) {
                setStars(parseInt(localStorage.getItem('ratedIndex')));
                uID = localStorage.getItem('uID');
            }

            $('.fa-star').on('click', function () {
               ratedIndex = parseInt($(this).data('index'));
               localStorage.setItem('ratedIndex', ratedIndex);
               saveToTheDB();
            });*/
            $('.fa-star').on('click', function () {
               ratedIndex = parseInt($(this).data('index'));
               rating = ratedIndex+1;
            });
            $('.fa-star').mouseover(function () {
                resetStarColors();
                var currentIndex = parseInt($(this).data('index'));
                setStars(currentIndex);
            });

            $('.fa-star').mouseleave(function () {
                resetStarColors();

                if (ratedIndex != -1)
                    setStars(ratedIndex);
            });

            $('#btnSubmit').click(function() {
                var review = $('textarea[name=taReview]').val();
                rating = parseInt(rating);
                var reviewObj = { productId: productId, review: review, rating: rating, orderId: orderId, orderItemId: orderitemId};

                $.ajax({ // FIXME: escape special characters using urlencode
                    url: "/Shopping/Review",
                    type:  "POST",
                    data: reviewObj,
                    error: function (jqxhr, status, errorThrown) {
                        toastr.error("Post failed, please try again");
                    }
                }).done(function () {
                    toastr.success("Review posted successfully");
                    // reset variables
                    ratedIndex = -1;
                    resetStarColors();
                    $('#reviewPane').hide();
                    $('textarea[name=taReview]').val("");
                    window.location.replace("/Shopping/Orderdetail/" + orderId);
                });
            });

            $('#btnCancel').click(function() {
                ratedIndex = -1;
                resetStarColors();
                $('#reviewPane').hide();
                $('textarea[name=taReview]').val("");
            });
        });

        function setStars(max) {
            for (var i=0; i <= max; i++)
                $('.fa-star:eq('+i+')').css('color', 'yellow');
        }

        function resetStarColors() {
            $('.fa-star').css('color', 'grey');
        }

        function showReviewPane(pId,url,name, oId, oiId) {
            resetStarColors();
            productId = pId;
            orderId = oId;
            orderitemId = oiId;
            console.log("orderItem id is " + orderitemId + ", productId is " + productId + ", orderId is " + orderId);
            $('#reviewPane').show();
            $('#productImg').html('<img width=\"150\" height=\"150\" alt=\"photo\" '
                    + 'src=' + url + '  style="display: inline-block;\">');
            $('#prodId').html(name);
            
        }
	