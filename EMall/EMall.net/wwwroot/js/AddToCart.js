﻿var starPercentageRounded;
var productId;
function getRatings(rating) {

    // Get percentage
    const starPercentage = (rating / 5) * 100;
    // Round to nearest 10
    starPercentageRounded = Math.round(starPercentage / 10) * 10 + "%";
    // Set width of stars-inner to percentage
    $('.stars-inner').css({ 'width': starPercentageRounded });
    //console.log(starPercentageRounded);
    // Add number rating
    //document.querySelector(`.${rating} .number-rating`).innerHTML = ratings[rating];
}
function getItemCount() {
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/CartItemQty",
        type: "GET",
        dataType: "json",
        error: function (jqxhr, status, errorThrown) {
            toastr.error("Get cart item quantity failed");
        }
    }).done(function (data) {
        var itemCount = jQuery.parseJSON(data);
        $('#itemCount').text(itemCount);
        //getSubtotal();
    });
}
function getSubtotal() {
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/CartItemSubtotal",
        type: "GET",
        dataType: "json",
        error: function (jqxhr, status, errorThrown) {
            toastr.error("Refresh subtotal failed");
        }
    }).done(function (data) {
        var subtotal = jQuery.parseJSON(data);
        $('#subtotal').text("$ " + subtotal);
    });
}
$(document).ready(function () {
    /* var cw = $('img').width();
     $('img').css({'height':cw+'px'});*/
    var rating = "{{ rating }}";
    // Total Stars
    getRatings(rating);
    $('.stars-inner').css({ 'width': starPercentageRounded });
    $('#number-rating').html(rating);

    //var modeljs = @Html.Raw(Json.Encode(Model.Index));
    $('#btnSubmit').click(function () {

        var quantity = parseInt($("input[name=quantity]").val());
        var cartitem = { id: productId, quant: quantity }; // "" is not necessary
        console.log("quantity is " + quantity + " id is " + productId);

        $.ajax({ // FIXME: escape special characters using urlencode
            url: "/Shopping/AddToCart",
            type: "POST",
            data: { id: productId, quant: quantity },
            error: function () {
                //alert("AJAX error: " + jqxhr.responseText);
                toastr.error("Add to cart failed");
            }
        }).done(function (data) {
            $('#addPanel').hide();
            getItemCount();
            getSubtotal();
            toastr.success(data.message);
            // var itemCount = jQuery.parseJSON(data);
            //alert("item count is " + itemCount);
            // reset variables

        $('input[name=quantity]').val("1");
        }).always(function () {
            $('#addPanel').hide();
        });

       

    });

    $('#btnCancel').click(function () {
        $('#addPanel').hide();
        $('input[name=quantity]').val("1");
    });
});
function addToCart(id) {
    var quantity = $("input[name=quantity]").val();
    console.log("quantity is " + quantity);
    console.log("id is " + id);
    var cartitem = { "ProductID": id, "Quantity": quantity }; // "" is not necessary

        var cartitemJson = JSON.stringify(cartitem);
        $.ajax({ // FIXME: escape special characters using urlencode
            url: "/Home/AddToCart2",
            type: "POST",
            dataType: "json",
            data: cartitemJson,
            error: function (data) {
                //alert("AJAX error: " + jqxhr.responseText);
                toastr.error("Add to cart failed");
            }
        }).done(function (data) {
            toastr.success(data.message);
            var itemCount = jQuery.parseJSON(data);
            //alert("item count is " + itemCount);
            // reset variables
            $('#addPanel').hide();
            $('input[name=quantity]').val("1");
            //$('#itemCount').text(itemCount);
        });

}
function showAddPanel(id, url, name, price) {
    productId = id;
    $('#addPanel').show();
    $('input[name=quantity]').val("1");
//    $("#btnSubmit").onclick = addToCart(id);
    console.log("id is " + id);
    //$('#fmAddToCart').attr("action", "Home/AddToCart/"+id);
   // $('#fmAddToCart').attr("asp-action", id);
    
        $('#productImg').html('<img width=\"150\" height=\"150\" alt=\"photo\" '
            + 'src=' + url + '  style="display: inline-block;\">');
        $('#prodId').html(name);
    
}
            /*function addToCart(productId) {

            var quantity = $("input[name='quantity"+productId+ "']").val();
            var cartitem = {"productId": productId, "quantity": quantity }; // "" is not necessary
        var cartitemJson = JSON.stringify(cartitem);
            $.ajax({ // FIXME: escape special characters using urlencode
            url: "/api/addtocart",
        type: "POST",
        dataType: "json",
        data: cartitemJson,
                error: function (jqxhr, status, errorThrown) {
            alert("AJAX error: " + jqxhr.responseText);
        }
            }).done(function (data) {
                var itemCount = jQuery.parseJSON(data);
        //alert("item count is " + itemCount);
        $("input[name='quantity"+productId+ "']").val("1");
        $('#itemCount').text(itemCount);
    });

    }*/
function getProductPagination(cateId, pageNum) {
    $("#productList").load("/Shopping/ProductPagination/"+cateId+"/"+pageNum);
    $(".pageNo").css("color", "orange");
    $('#pageNo' + pageNum).css("color", "white");
    $(".pageNo").css("background-color", "white");
    $('#pageNo' + pageNum).css("background-color", "orange");
}