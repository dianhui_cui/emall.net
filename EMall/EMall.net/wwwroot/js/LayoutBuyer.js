﻿function getItemCount() {
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/CartItemQty",
        type: "GET",
        dataType: "json",
        error: function (jqxhr, status, errorThrown) {
            alert("AJAX error: " + jqxhr.responseText);
        }
    }).done(function (data) {
        var itemCount = jQuery.parseJSON(data);
        $('#itemCount').text(itemCount);
        //getSubtotal();
    });
}
function getSubtotal() {
    $.ajax({ // FIXME: escape special characters using urlencode
        url: "/Shopping/CartItemSubtotal",
        type: "GET",
        dataType: "json",
        error: function (jqxhr, status, errorThrown) {
            alert("AJAX error: " + jqxhr.responseText);
        }
    }).done(function (data) {
        var subtotal = jQuery.parseJSON(data);
        $('#subtotal').text("$ " + subtotal);
    });
}
$(document).ready(function () {
    getItemCount();
    getSubtotal();
});