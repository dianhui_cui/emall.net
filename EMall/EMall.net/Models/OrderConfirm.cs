﻿using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class OrderConfirm
    {
        public Dictionary<int, List<CartItemView>> allCartItems { get; set; }
        public int buyerId { get; set; }
        public String totalBeforeTax { get; set; }
        public String shippingBeforeTax { get; set; }
        public String taxes { get; set; }
        public String totalWithShippingAndTaxes { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String Email { get; set; }
        public String PhoneNo { get; set; }
        public String City { get; set; }
        public String Province { get; set; }
        public String PostalCode { get; set; }

    }
            
}
