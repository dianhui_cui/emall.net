﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class ProductList
    {
        public List<MdlComProductCategory> AllProductCategories { get; set; }
        public List<ProductView> AllProducts { get; set; }
        public int PageNum { get; set; }
        public int? CategoryID { get; set; }
    }
}
