﻿using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlBuyerOrder
    {
        public int ID { get; set; }
        public int BuyerID { get; set; }
        public int SellerID { get; set; }
        public System.DateTime OrderTime { get; set; }
        public decimal TotalBeforeTax { get; set; }
        public decimal ShippingBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal Total { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public string ShippingAddress { get; set; }
        public string Postalcode { get; set; }
        public string RecieverName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        List<MdlBuyerOrderItem> items;
        public List<MdlBuyerOrderItem> OrderItems { get

            {
                if (null == items)
                {
                    List<OrderItemView> views = OrderOperator.Single.FindOrderItemByOrder(this.ID);
                    items = new List<MdlBuyerOrderItem>();
                    foreach (var item in views)
                    {
                        items.Add(new MdlBuyerOrderItem() {
                            ID = item.OrderItem.ID,
                            BuyCount = item.OrderItem.BuyCount,
                            BuyPrice = item.OrderItem.BuyPrice,
                            OrderID = item.OrderItem.OrderID,
                            PictureUrl = item.PictureUrl,
                            ProductID = item.OrderItem.ProductID,
                            ProductName = item.ProductName,
                            Subtotal = item.OrderItem.Subtotal,
                            UnitPrice = item.OrderItem.UnitPrice,
                            Reviewed=item.OrderItem.Reviewed
                        });
                    }
                }

                return items;
            }

        }
        public  User Buyer { get { return UserOperator.Single.Find(this.BuyerID); } }
        public  User Seller { get { return UserOperator.Single.Find(this.SellerID); } }

        public string SellerName { get; set; }
        public string BuyerName { get; set; }

    }
}
