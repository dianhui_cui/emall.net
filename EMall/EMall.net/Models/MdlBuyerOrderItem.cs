﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlBuyerOrderItem
    {
        public int ID { get; set; }

        public int OrderID { get; set; }

        public int ProductID { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal BuyPrice { get; set; }

        public decimal BuyCount { get; set; }

        public decimal Subtotal { get; set; }



        public string ProductName { get; set; }
        public string PictureUrl { get; set; }
        public int Reviewed { get; set; }
    }
}
