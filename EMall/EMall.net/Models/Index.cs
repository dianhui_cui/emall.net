﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class Index
    {
        public List<MdlComProductCategory> allProductCategories { get; set; }
        public List<ProductView> allProducts { get; set; }

        public int quantity { get; set; }
    }
}
