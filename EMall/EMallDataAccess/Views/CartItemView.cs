﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Views
{
    public class CartItemView
    {
        public CartItem CartItem { get; set; }
        public string ProductName { get; set; }
        public string SellerName { get; set; }
        public string BuyerName { get; set; }
        public decimal UnitPrice { get; set; }
        public string PictureUrl { get; set; }
    }
}
