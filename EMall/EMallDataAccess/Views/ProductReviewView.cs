﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Views
{
    public class ProductReviewView
    {
        public ProductReview ProductReview { get; set; }
        public string ProductName { get; set; }
        public string BuyerName { get; set; }
    }
}
