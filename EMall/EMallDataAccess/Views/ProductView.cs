﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Views
{
    public class ProductView
    {
        public Product Product { get; set; }
        public string SellerName { get; set; }
        public string CategoryName { get; set; }
    }
}
