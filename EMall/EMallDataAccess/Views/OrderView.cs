﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Views
{
    public class OrderView
    {
        public Order Order { get; set; }
        public string SellerName { get; set; }
        public string BuyerName { get; set; }

    }
}
