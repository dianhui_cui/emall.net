﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMallDataAccess.DataAccess
{
    public class CartItemOperator
    {
        private static CartItemOperator productReviewOperator;
        private static object mLock = new object();
        public static CartItemOperator Single
        {
            get
            {
                lock (mLock)
                {
                    if (productReviewOperator == null)
                    {
                        productReviewOperator = new CartItemOperator();

                    }
                    return productReviewOperator;
                }
            }
        }
        public void Add(CartItem item)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                dbContext.CartItems.Add(item);
                dbContext.SaveChanges();
            }
        }
        public void Remove(CartItem item)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem obj = dbContext.CartItems.Find(item.ID);
                dbContext.CartItems.Remove(obj);
                dbContext.SaveChanges();
            }
        }
        public void Add(int buyerID, int productID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    Product product = dbContext.Products.Find(productID);
                    cartItem = new CartItem()
                    {
                        BuyerID = buyerID,
                        ProductID = productID,
                        Quantity = 1,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = string.Empty,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += 1;
                }
                dbContext.SaveChanges();
            }
        }
        public void Add(int buyerID, int productID, decimal quantity)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    Product product = dbContext.Products.Find(productID);
                    cartItem = new CartItem()
                    {
                        BuyerID = buyerID,
                        ProductID = productID,
                        Quantity = quantity,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = string.Empty,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += quantity;
                }
                dbContext.SaveChanges();
            }
        }

        public void Add(string sessionID, int productID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    Product product = dbContext.Products.Find(productID);
                    cartItem = new CartItem()
                    {

                        ProductID = productID,
                        Quantity = 1,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = sessionID,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += 1;
                }
                dbContext.SaveChanges();
            }
        }
        public void Add(string sessionID, int productID, decimal quantity)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    Product product = dbContext.Products.Find(productID);
                    cartItem = new CartItem()
                    {

                        ProductID = productID,
                        Quantity = quantity,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = sessionID,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += quantity;
                }
                dbContext.SaveChanges();
            }
        }

        public void UpdateQty(int ciID, decimal quantity)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.ID == ciID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {

                }
                else
                {
                    if (quantity == 0)
                    {
                        dbContext.CartItems.Remove(cartItem);
                    }
                    else
                    {
                        cartItem.Quantity = quantity;
                    }
                }
                dbContext.SaveChanges();
            }
        }
        public void Update(int ciID, int status)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.ID == ciID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {

                }
                else
                {
                    cartItem.Status = (byte)status;
                }
                dbContext.SaveChanges();
            }
        }

        public void UpdateStore(int stID, int buyerID, int status)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {

                List<CartItem> list = new List<CartItem>();

                try
                {
                    list = dbContext.CartItems.Where(ci => ci.SellerID == stID && ci.BuyerID == buyerID).ToList();
                }
                catch (ArgumentNullException ex)
                {

                }

                if (null == list)
                {

                }
                else
                {
                    foreach (var ci in list)
                    {
                        ci.Status = (byte)status;
                    }
                }
                dbContext.SaveChanges();
            }
        }
        public void UpdateAll(int buyerID, int status)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {

                List<CartItem> list = new List<CartItem>();

                try
                {
                    list = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                }
                catch (ArgumentNullException ex)
                {

                }

                if (null == list)
                {

                }
                else
                {
                    foreach (var ci in list)
                    {
                        ci.Status = (byte)status;
                    }
                }
                dbContext.SaveChanges();
            }
        }
        public void TransferCartItems(string sessionID, int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItem> cartItems = new List<CartItem>();
                try
                {
                    cartItems = dbContext.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItem>();
                }
                foreach (var item in cartItems)
                {
                    dbContext.CartItems.Remove(item);
                }
                foreach (var item in cartItems)
                {

                    Add(buyerID, item.ProductID, item.Quantity);
                }
            }
        }
        public CartItemView Find(int ID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                CartItemView cartItem = new CartItemView();
                try
                {
                    var data = dbContext.CartItems.Where(ci => ci.ID == ID).FirstOrDefault();
                    cartItem = new CartItemView()
                    {
                        CartItem = data,
                        SellerName = string.IsNullOrWhiteSpace(data.Seller.FirstName) ? data.Seller.Username : string.Format("{0} {1}", data.Seller.FirstName, data.Seller.LastName),
                        ProductName = data.Product.Name,
                        UnitPrice = data.Product.UnitPrice,
                        PictureUrl = data.Product.PictureUrl,
                        BuyerName = string.IsNullOrWhiteSpace(data.Buyer.FirstName) ? data.Buyer.Username : string.Format("{0} {1}", data.Buyer.FirstName, data.Buyer.LastName)
                    };

                }
                catch (ArgumentNullException ex)
                {
                    cartItem = new CartItemView();
                }
                return cartItem;
            }
        }
        public List<CartItemView> FindByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            UnitPrice = item.Product.UnitPrice,
                            PictureUrl = item.Product.PictureUrl,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }
                return cartItems;
            }
        }

        public List<CartItemView> FindSelectedByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID && ci.Status == 1).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            UnitPrice = item.Product.UnitPrice,
                            PictureUrl = item.Product.PictureUrl,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }
                return cartItems;
            }
        }
        public int FindQtyByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            UnitPrice = item.Product.UnitPrice,
                            PictureUrl = item.Product.PictureUrl,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }

                return cartItems.Count();
            }
        }
        public async Task<int> FindQtyByBuyerAsyn(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            UnitPrice = item.Product.UnitPrice,
                            PictureUrl = item.Product.PictureUrl,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }

                return cartItems.Count();
            }
        }
        public string FindSubtotalByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                decimal subtotal = 0;
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (CartItem ci in datas)
                    {
                        Product p = ProductOperator.Single.Find(ci.ProductID);
                        subtotal += ci.Quantity * p.UnitPrice;
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                    subtotal = 0;
                }

                return string.Format("{0:0.00}", subtotal);
            }
        }
        public decimal FindSelectedSubtotalByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                decimal subtotal = 0;
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.BuyerID == buyerID && ci.Status == 1).ToList();
                    foreach (CartItem ci in datas)
                    {
                        Product p = ProductOperator.Single.Find(ci.ProductID);
                        subtotal += ci.Quantity * p.UnitPrice;
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                    subtotal = 0;
                }

                return decimal.Round(subtotal, 2, MidpointRounding.AwayFromZero);
            }
        }
        public List<CartItemView> FindBySession(string sessionID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas = dbContext.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            UnitPrice = item.Product.UnitPrice,
                            BuyerName = sessionID
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }
                return cartItems;
            }
        }
        #region backup single mode
        /*
        public void Add(CartItem item)
        {
            EMallDbContext.Single.CartItems.Add(item);
            EMallDbContext.Single.SaveChanges();
        }
        public void Remove(CartItem item)
        {
            EMallDbContext.Single.CartItems.Remove(item);
            EMallDbContext.Single.SaveChanges();
        }
        public void Add(int buyerID, int productID)
        {
            CartItem cartItem = null;
            try 
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {
                Product product = EMallDbContext.Single.Products.Find(productID);
                cartItem = new CartItem()
                {
                    BuyerID = buyerID,
                    ProductID = productID,
                    Quantity = 1,
                    SellerID = product.SellerID,
                    CreationTS = DateTime.Now,
                    SessionID = string.Empty,
                    Status=1
                };
                EMallDbContext.Single.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity += 1;
            }
            EMallDbContext.Single.SaveChanges();
        }
        public void Add(int buyerID, int productID,decimal quantity)
        {
            CartItem cartItem = null;
            try
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {
                Product product = EMallDbContext.Single.Products.Find(productID);
                cartItem = new CartItem()
                {
                    BuyerID = buyerID,
                    ProductID = productID,
                    Quantity = quantity,
                    SellerID = product.SellerID,
                    CreationTS = DateTime.Now,
                    SessionID = string.Empty,
                    Status = 1
                };
                EMallDbContext.Single.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity += quantity;
            }
            EMallDbContext.Single.SaveChanges();
        }

        public void Add(string sessionID, int productID)
        {
            CartItem cartItem = null;
            try
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {
                Product product = EMallDbContext.Single.Products.Find(productID);
                cartItem = new CartItem()
                {
                    
                    ProductID = productID,
                    Quantity = 1,
                    SellerID = product.SellerID,
                    CreationTS = DateTime.Now,
                    SessionID = sessionID,
                    Status = 1
                };
                EMallDbContext.Single.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity += 1;
            }
            EMallDbContext.Single.SaveChanges();
        }
        public void Add(string sessionID, int productID, decimal quantity)
        {
            CartItem cartItem = null;
            try
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {
                Product product = EMallDbContext.Single.Products.Find(productID);
                cartItem = new CartItem()
                {

                    ProductID = productID,
                    Quantity = quantity,
                    SellerID = product.SellerID,
                    CreationTS = DateTime.Now,
                    SessionID = sessionID,
                    Status = 1
                };
                EMallDbContext.Single.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity += quantity;
            }
            EMallDbContext.Single.SaveChanges();
        }

        public void UpdateQty(int ciID, decimal quantity)
        {
            CartItem cartItem = null;
            try
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.ID == ciID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {
                
            }
            else
            {
                if (quantity == 0)
                {
                    EMallDbContext.Single.CartItems.Remove(cartItem);
                }
                else
                {
                    cartItem.Quantity = quantity;
                }
            }
            EMallDbContext.Single.SaveChanges();
        }
        public void Update(int ciID, int status)
        {
            CartItem cartItem = null;
            try
            {
                cartItem = EMallDbContext.Single.CartItems.FirstOrDefault(ci => ci.ID == ciID);
            }
            catch (ArgumentNullException ex)
            {
                cartItem = null;
            }

            if (null == cartItem)
            {

            }
            else
            {
                cartItem.Status = (byte)status;
            }
            EMallDbContext.Single.SaveChanges();
        }

        public void UpdateStore(int stID, int buyerID, int status)
        {


            List<CartItem> list = new List<CartItem>();

            try
            {
                list = EMallDbContext.Single.CartItems.Where(ci => ci.SellerID == stID && ci.BuyerID == buyerID).ToList();
            }
            catch (ArgumentNullException ex)
            {
                
            }

            if (null == list)
            {

            }
            else
            {
                foreach (var ci in list)
                {
                    ci.Status = (byte)status;
                }
            }
            EMallDbContext.Single.SaveChanges();
        }
        public void UpdateAll(int buyerID, int status)
        {


            List<CartItem> list = new List<CartItem>();

            try
            {
                list = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
            }
            catch (ArgumentNullException ex)
            {

            }

            if (null == list)
            {

            }
            else
            {
                foreach (var ci in list)
                {
                    ci.Status = (byte)status;
                }
            }
            EMallDbContext.Single.SaveChanges();
        }
        public void TransferCartItems(string sessionID, int buyerID)
        {
            List<CartItem> cartItems = new List<CartItem>();
            try
            {
                cartItems = EMallDbContext.Single.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItem>();
            }
            foreach (var item in cartItems)
            {
                EMallDbContext.Single.CartItems.Remove(item);
            }
                foreach (var item in cartItems)
            {

                Add(buyerID, item.ProductID, item.Quantity);
            }
        }
        public CartItemView Find(int ID)
        {
            CartItemView cartItem = new CartItemView();
            try
            {
                var data = EMallDbContext.Single.CartItems.Where(ci => ci.ID == ID).FirstOrDefault();
                cartItem = new CartItemView()
                    {
                        CartItem = data,
                        SellerName = string.IsNullOrWhiteSpace(data.Seller.FirstName) ? data.Seller.Username : string.Format("{0} {1}", data.Seller.FirstName, data.Seller.LastName),
                        ProductName = data.Product.Name,
                        UnitPrice = data.Product.UnitPrice,
                        PictureUrl = data.Product.PictureUrl,
                        BuyerName = string.IsNullOrWhiteSpace(data.Buyer.FirstName) ? data.Buyer.Username : string.Format("{0} {1}", data.Buyer.FirstName, data.Buyer.LastName)
                    };
                
            }
            catch (ArgumentNullException ex)
            {
                cartItem = new CartItemView();
            }
            return cartItem;
        }
        public List<CartItemView> FindByBuyer(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                foreach (var item in datas)
                {
                    cartItems.Add(new CartItemView() {
                        CartItem = item,
                        SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                        ProductName = item.Product.Name,
                        UnitPrice = item.Product.UnitPrice,
                        PictureUrl = item.Product.PictureUrl,
                        BuyerName =string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                    });
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
            }
            return cartItems;
        }

        public List<CartItemView> FindSelectedByBuyer(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID && ci.Status==1).ToList();
                foreach (var item in datas)
                {
                    cartItems.Add(new CartItemView()
                    {
                        CartItem = item,
                        SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                        ProductName = item.Product.Name,
                        UnitPrice = item.Product.UnitPrice,
                        PictureUrl = item.Product.PictureUrl,
                        BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                    });
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
            }
            return cartItems;
        }
        public int FindQtyByBuyer(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                foreach (var item in datas)
                {
                    cartItems.Add(new CartItemView()
                    {
                        CartItem = item,
                        SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                        ProductName = item.Product.Name,
                        UnitPrice = item.Product.UnitPrice,
                        PictureUrl = item.Product.PictureUrl,
                        BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                    });
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
            }
            
            return cartItems.Count();
        }
        public async Task<int> FindQtyByBuyerAsyn(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                foreach (var item in datas)
                {
                    cartItems.Add(new CartItemView()
                    {
                        CartItem = item,
                        SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                        ProductName = item.Product.Name,
                        UnitPrice = item.Product.UnitPrice,
                        PictureUrl = item.Product.PictureUrl,
                        BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                    });
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
            }

            return cartItems.Count();
        }
        public string FindSubtotalByBuyer(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            decimal subtotal = 0;
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                foreach (CartItem ci in datas)
                {
                    Product p = ProductOperator.Single.Find(ci.ProductID);
                    subtotal += ci.Quantity * p.UnitPrice;
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
                subtotal = 0;
            }
            
            return string.Format("{0:0.00}",subtotal);
        }
        public decimal FindSelectedSubtotalByBuyer(int buyerID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            decimal subtotal = 0;
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.BuyerID == buyerID && ci.Status == 1).ToList();
                foreach (CartItem ci in datas)
                {
                    Product p = ProductOperator.Single.Find(ci.ProductID);
                    subtotal += ci.Quantity * p.UnitPrice;
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
                subtotal = 0;
            }

            return decimal.Round(subtotal, 2, MidpointRounding.AwayFromZero);
        }
        public List<CartItemView> FindBySession(string  sessionID)
        {
            List<CartItemView> cartItems = new List<CartItemView>();
            try
            {
                var datas = EMallDbContext.Single.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
                foreach (var item in datas)
                {
                    cartItems.Add(new CartItemView()
                    {
                        CartItem = item,
                        SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                        ProductName = item.Product.Name,
                        UnitPrice = item.Product.UnitPrice,
                        BuyerName = sessionID
                    });
                }
            }
            catch (ArgumentNullException ex)
            {
                cartItems = new List<CartItemView>();
            }
            return cartItems;
        }
        */
        #endregion
    }
}
