﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class DbHelper
    {
        private static string ConnectionString;
        private static readonly object objLock = new object(); 
        public static string GetConnectionString()
        {
            lock (objLock)
            {
                if (string.IsNullOrWhiteSpace(ConnectionString))
                {
                    IConfigurationBuilder builder = new ConfigurationBuilder();
                    builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"));


                    var root = builder.Build();
                    var providerEnum = root.Providers.GetEnumerator();
                    providerEnum.MoveNext();
                    IConfigurationProvider provider = providerEnum.Current;

                    string host = string.Empty;
                    string database = string.Empty;
                    string username = string.Empty;
                    string password = string.Empty;
                    provider.TryGet("EMallDbConfig:Host", out host);
                    provider.TryGet("EMallDbConfig:Database", out database);
                    provider.TryGet("EMallDbConfig:Username", out username);
                    provider.TryGet("EMallDbConfig:Password", out password);
                    //root.GetSection("EMallDbConfig").Value;

                    ConnectionString = string.Format("Server={0};Database={1};User Id={2};Password={3};", host, database, username, password);
                }
                return ConnectionString;
            }
        }
    }
}
