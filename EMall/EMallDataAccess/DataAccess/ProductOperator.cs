﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class ProductOperator
    {
        private static ProductOperator productOperator;
        public static ProductOperator Single
        {
            get
            {
                if (productOperator == null)
                {
                    productOperator = new ProductOperator();

                }
                return productOperator;
            }
        }
        public void Add(Product product)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                dbContext.Products.Add(product);
                dbContext.SaveChanges();
            }
        }
        public void Update(Product product)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                Product obj = dbContext.Products.Find(product.ID);
                obj.CategoryID = product.CategoryID;
                obj.Description = product.Description;
                obj.Discount = product.Discount;
                obj.Inventory = product.Inventory;
                obj.Name = product.Name;
                obj.PictureUrl = product.PictureUrl;
                obj.Rating = product.Rating;
                obj.ReviewCount = product.ReviewCount;
                obj.SalesCount = product.SalesCount;
                obj.Unit = product.Unit;
                obj.UnitPrice = product.UnitPrice;

                dbContext.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                Product product = dbContext.Products.Find(id);
                if (null != product)
                {
                    dbContext.Products.Remove(product);
                    dbContext.SaveChanges();
                }
            }
        }
        public Product Find(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                Product product = dbContext.Products.Find(id);
                return product;
            }
        }
        public int FindCountAll()
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.Products.Count();
            }
        }
        public List<ProductView> FindAll()
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindAll(int pageCount,int skipCount)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Skip(skipCount).Take(pageCount).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public int FindCountByCategory(int categoryID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.Products.Where(p => p.CategoryID == categoryID).Count();
            }
        }
        public List<ProductView> FindByCategory(int categoryID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {

                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.CategoryID == categoryID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindByCategory(int categoryID,int pageCount, int skipCount)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.CategoryID == categoryID).Skip(skipCount).Take(pageCount).ToList();
                    //var datas = dbContext.Products.Where(p => p.CategoryID == categoryID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
    
        }

        public int FindCountBySeller(int sellerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.Products.Where(p => p.SellerID == sellerID).Count();
            }
        }
        public List<ProductView> FindSeller(int sellerID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.SellerID == sellerID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindSeller(int sellerID,int pageCount, int skipCount)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.SellerID == sellerID).Take(pageCount).Skip(skipCount).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindByName(string name)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.Name.Contains(name)).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        #region backup single mode
        /*
             public void Add(Product product)
        {
            EMallDbContext.Single.Products.Add(product);
            EMallDbContext.Single.SaveChanges();
        }
        public void Update(Product product)
        {
            EMallDbContext.Single.SaveChanges();
        }
        public void Delete(int id)
        {
            Product product = EMallDbContext.Single.Products.Find(id);
            if (null != product)
            {
                EMallDbContext.Single.Products.Remove(product);
                EMallDbContext.Single.SaveChanges();
            }
        }
        public Product Find(int id)
        {
            Product product = EMallDbContext.Single.Products.Find(id);
            return product;
        }
        public int FindCountAll()
        {
            return EMallDbContext.Single.Products.Count();
        }
        public List<ProductView> FindAll()
        {
            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }
               
            }
            catch (ArgumentNullException ex)
            { 
                products = new List<ProductView>();
            }
          return  products;
        }
        public List<ProductView> FindAll(int pageCount,int skipCount)
        {
            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Skip(skipCount).Take(pageCount).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
        }
        public int FindCountByCategory(int categoryID)
        {
            return EMallDbContext.Single.Products.Where(p=>p.CategoryID==categoryID).Count();
        }
        public List<ProductView> FindByCategory(int categoryID)
        {


            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Where(p => p.CategoryID == categoryID).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
        }
        public List<ProductView> FindByCategory(int categoryID,int pageCount, int skipCount)
        {

            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Where(p => p.CategoryID == categoryID).Skip(skipCount).Take(pageCount).ToList();
                //var datas = EMallDbContext.Single.Products.Where(p => p.CategoryID == categoryID).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
    
        }

        public int FindCountBySeller(int sellerID)
        {
            return EMallDbContext.Single.Products.Where(p => p.SellerID == sellerID).Count();
        }
        public List<ProductView> FindSeller(int sellerID)
        {
            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Where(p => p.SellerID == sellerID).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
        }
        public List<ProductView> FindSeller(int sellerID,int pageCount, int skipCount)
        {
             List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Where(p => p.SellerID == sellerID).Take(pageCount).Skip(skipCount).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
        }
        public List<ProductView> FindByName(string name)
        {
            List<ProductView> products = new List<ProductView>();
            try
            {
                var datas = EMallDbContext.Single.Products.Where(p => p.Name.Contains(name)).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductView>();
            }
            return products;
        }
         */
        #endregion
    }
}
