﻿using EMallDataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class EMallDbContext : DbContext
    {
        private static EMallDbContext context;
        private static object objLock = new object();
        public static EMallDbContext Single
        {
            get
            {
              
                lock (objLock)
                {
                    if (null == context)
                    {
                        DbContextOptions options = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), DbHelper.GetConnectionString()).Options;
                        EMallDbContext dbContext = new EMallDbContext(options);
                        context = dbContext;
                    }
                    return context;
                }



            }
        }
        public static EMallDbContext NewInstance
        {
            get
            {
                DbContextOptions options = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), DbHelper.GetConnectionString()).Options;
                return new EMallDbContext(options);
            }
        }
        public EMallDbContext(DbContextOptions options) : base(options) { 
           
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
         .UseLazyLoadingProxies()
         .UseSqlServer(DbHelper.GetConnectionString());
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbQuery<CustomerSaleStatistic> CustomerSaleStatistics { get; set; }
        public DbQuery<OrderSaleStatistic> OrderSaleStatistics { get; set; }
        public DbQuery<ProductSaleStatistic> ProductSaleStatistics { get; set; }
        public DbQuery<OrderCountStatistic> OrderCountStatistics { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CartItem>().HasOne(c => c.Buyer).WithMany(u => u.BuyerCartItems);
            modelBuilder.Entity<CartItem>().HasOne(c => c.Seller).WithMany(u => u.SellerCartItems);
            modelBuilder.Entity<CartItem>().HasOne(c => c.Product).WithMany(p=>p.CartItems);
            modelBuilder.Entity<Order>().HasOne(o => o.Buyer).WithMany(u => u.BuyerOrders);
            modelBuilder.Entity<Order>().HasOne(o => o.Seller).WithMany(u => u.SellerOrders);
            modelBuilder.Entity<OrderItem>().HasOne(oi => oi.Order).WithMany(o => o.OrderItems);
            modelBuilder.Entity<OrderItem>().HasOne(oi => oi.Product).WithMany(p => p.OrderItems);
            modelBuilder.Entity<Product>().HasOne(p => p.ProductCategory).WithMany(pc => pc.Products);
            modelBuilder.Entity<Product>().HasOne(p => p.Seller).WithMany(u => u.Products);
            modelBuilder.Entity<ProductCategory>().HasOne(pc => pc.ParentCategory).WithMany(pc => pc.ChildProductCategories);
            modelBuilder.Entity<ProductReview>().HasOne(pr => pr.Buyer).WithMany(u => u.ProductReviews);
            modelBuilder.Entity<ProductReview>().HasOne(pr => pr.Product).WithMany(p=>p.ProductReviews);
            modelBuilder.Entity<ProductReview>().HasOne(pr => pr.Order).WithMany(o => o.ProductReviews);
        }
    }
}
