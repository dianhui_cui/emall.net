﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class ProductCategoryOperator
    {
        private static ProductCategoryOperator productCategoryOperator;
        public static ProductCategoryOperator Single
        {
            get
            {
                if (productCategoryOperator == null)
                {
                    productCategoryOperator = new ProductCategoryOperator();

                }
                return productCategoryOperator;
            }
        }
        public void Add(ProductCategory category)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
               dbContext.ProductCategories.Add(category);
               dbContext.SaveChanges();
            }
        }
        public void Update(ProductCategory category)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                ProductCategory obj = dbContext.ProductCategories.Find(category.ID);
                obj.Name = category.Name;
               dbContext.SaveChanges();
            }
        }
        public ProductCategory Find(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.ProductCategories.Find(id);
            }
        }
        public List<ProductCategory> FindAll()
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.ProductCategories.ToList();
            }
        }
        public List<ProductCategory> FindTopCategories()
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductCategory> productCategories = new List<ProductCategory>();
                try
                {
                    productCategories =dbContext.ProductCategories.Where(c => c.ParentCategory == null).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    productCategories = new List<ProductCategory>();
                }

                return productCategories;
            }
        }
        public List<ProductCategory> FindChildCategories(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductCategory> productCategories = new List<ProductCategory>();
                try
                {
                    productCategories =dbContext.ProductCategories.Where(pc => pc.ParentID == id).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    productCategories = new List<ProductCategory>();
                }

                return productCategories;
            }
        }
        #region backup single mode
        /*
               public void Add(ProductCategory category)
        {
            EMallDbContext.Single.ProductCategories.Add(category);
            EMallDbContext.Single.SaveChanges();
        }
        public void Update(ProductCategory category)
        {
            EMallDbContext.Single.SaveChanges();
        }
        public ProductCategory Find(int id)
        {
            return EMallDbContext.Single.ProductCategories.Find(id);
        }
        public List<ProductCategory> FindAll()
        {
            return EMallDbContext.Single.ProductCategories.ToList();
        }
        public List<ProductCategory> FindTopCategories()
        {
            List<ProductCategory> productCategories = new List<ProductCategory>();
            try {
                productCategories = EMallDbContext.Single.ProductCategories.Where(c => c.ParentCategory == null).ToList();
            }
            catch (ArgumentNullException ex)
            {
                productCategories = new List<ProductCategory>();
            }

            return productCategories;
        }
        public List<ProductCategory> FindChildCategories(int id)
        {
            List<ProductCategory> productCategories = new List<ProductCategory>();
            try
            {
                productCategories = EMallDbContext.Single.ProductCategories.Where(pc=>pc.ParentID==id).ToList();
            }
            catch (ArgumentNullException ex)
            {
                productCategories = new List<ProductCategory>();
            }

            return productCategories;
        }
         */
        #endregion
    }
}
