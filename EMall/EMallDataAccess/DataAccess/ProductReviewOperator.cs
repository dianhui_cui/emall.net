﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class ProductReviewOperator
    {
        private static ProductReviewOperator productReviewOperator;
        public static ProductReviewOperator Single
        {
            get
            {
                if (productReviewOperator == null)
                {
                    productReviewOperator = new ProductReviewOperator();

                }
                return productReviewOperator;
            }
        }
        public void Add(ProductReview productReview)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                dbContext.ProductReviews.Add(productReview);
                dbContext.SaveChanges();
            }
        }
        public void Update(ProductReview review)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                ProductReview obj = dbContext.ProductReviews.Find(review.ID);
                obj.Rating = review.Rating;
                obj.Review = review.Review;
                dbContext.SaveChanges();
            }
        }
        public ProductReview Find(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.ProductReviews.Find(id);
            }
        }
        public List<ProductReviewView> FindByProduct(int productID)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductReviewView> products = new List<ProductReviewView>();
                try
                {
                    var datas = dbContext.ProductReviews.Where(pr => pr.ProductID == productID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductReviewView()
                        {
                            ProductReview = item,
                            ProductName = item.Product.Name,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductReviewView>();
                }
                return products;
            }
        }
        #region backup single mode
        /*
              public void Add(ProductReview productReview)
        {
            EMallDbContext.Single.ProductReviews.Add(productReview);
            EMallDbContext.Single.SaveChanges();
        }
        public void Update(ProductReview review)
        {
            EMallDbContext.Single.SaveChanges();
        }
        public ProductReview Find(int id)
        {
            return EMallDbContext.Single.ProductReviews.Find(id);
        }
        public List<ProductReviewView> FindByProduct(int productID)
        {
            List<ProductReviewView> products = new List<ProductReviewView>();
            try
            {
                var datas = EMallDbContext.Single.ProductReviews.Where(pr=>pr.ProductID==productID).ToList();
                foreach (var item in datas)
                {
                    products.Add(new ProductReviewView() { 
                        ProductReview = item, 
                        ProductName=item.Product.Name,
                        BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                    });
                }

            }
            catch (ArgumentNullException ex)
            {
                products = new List<ProductReviewView>();
            }
            return products;
        }
         */
        #endregion

    }
}
