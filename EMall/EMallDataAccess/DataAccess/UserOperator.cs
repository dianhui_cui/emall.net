﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class UserOperator
    {
        private static UserOperator userOperator;
        public static UserOperator Single
        {
            get {
                if (userOperator == null)
                {
                    userOperator = new UserOperator();

                }
                return userOperator;
            }
        }
    
        public void Add(User user)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }
        }
        public void Update(User user)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                User obj = dbContext.Users.Find(user.ID);
                obj.LastName = user.LastName;
                obj.Address = user.Address;
                obj.City = user.City;
                obj.Country = user.Country;
                obj.Description = user.Description;

                obj.FirstName = user.FirstName;
                obj.MiddleName = user.MiddleName;
                obj.OrderCount = user.OrderCount;
                obj.Password = user.Password;
                obj.PhoneNo = user.PhoneNo;
                obj.PostalCode = user.PostalCode;
                obj.Province = user.Province;
                obj.RegisterTime = user.RegisterTime;
                obj.Score = user.Score;
                obj.ShopPage = user.ShopPage;

                dbContext.SaveChanges();
            }
        }
        public User Find(int id)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                return dbContext.Users.Find(id);
            }
        }
        public User Login(string username, string password,UserType userType)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                User user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Username == username && u.Password == password && u.UserType == userType);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user;
            }
        }
        public bool IsUsernameTaken(string username, int id=0)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                User user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Username == username && u.ID != id);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user != null;
            }
        }
        public bool IsEmailTaken(string email, int id = 0)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                User user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Email == email && u.ID != id);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user != null;
            }
        }

        #region backup single mode
        /*
             public void Add(User user)
        {
            EMallDbContext.Single.Users.Add(user);
            EMallDbContext.Single.SaveChanges();
        }
        public void Update(User user)
        {
            EMallDbContext.Single.SaveChanges();
        }
        public User Find(int id)
        {
            return EMallDbContext.Single.Users.Find(id);
        }
        public User Login(string username, string password,UserType userType)
        {
            User user = null;
            try
            {
                user = EMallDbContext.Single.Users.FirstOrDefault(u => u.Username == username && u.Password == password&&u.UserType== userType);
            }
            catch (ArgumentNullException ex)
            {
                user = null;
            }
            return user;
        }
        public bool IsUsernameTaken(string username, int id=0)
        {

            User user = null;
            try
            {
                user = EMallDbContext.Single.Users.FirstOrDefault(u => u.Username == username && u.ID !=id );
            }
            catch (ArgumentNullException ex)
            {
                user = null;
            }
            return user!=null;
        }
        public bool IsEmailTaken(string email, int id = 0)
        {
            User user = null;
            try
            {
                user = EMallDbContext.Single.Users.FirstOrDefault(u => u.Email == email && u.ID != id);
            }
            catch (ArgumentNullException ex)
            {
                user = null;
            }
            return user != null;
        }
         */
        #endregion



    }

}
