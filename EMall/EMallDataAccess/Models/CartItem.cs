﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDataAccess.Models
{
    [Table("CartItems", Schema = "emall")]
    public class CartItem
    {
        [Key]
        public int ID { get; set; }
        [MaxLength(50)]
        public string SessionID { get; set; }
        public Nullable<int> BuyerID { get; set; }
        public Nullable<int> SellerID { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public decimal Quantity { get; set; }
        [Required]
        public System.DateTime CreationTS { get; set; }
        [Required]
        public byte Status { get; set; }
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }
        [ForeignKey("BuyerID")]
        public virtual User Buyer { get; set; }
        [ForeignKey("SellerID")]
        public virtual User Seller { get; set; }
    }
}
