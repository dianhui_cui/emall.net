﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDataAccess.Models
{
    [Table("Orders", Schema = "emall")]
    public class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.OrderItems = new HashSet<OrderItem>();
            this.ProductReviews = new HashSet<ProductReview>();
        }
        [Key]
        public int ID { get; set; }
        [Required]
        public int BuyerID { get; set; }
        [Required]
        public int SellerID { get; set; }
        [Required]
        public System.DateTime OrderTime { get; set; }
        [Required]
        public decimal TotalBeforeTax { get; set; }
        [Required]
        public decimal ShippingBeforeTax { get; set; }
        [Required]
        public decimal Taxes { get; set; }
        [Required]
        public decimal Total { get; set; }
        [Required]
         public OrderStatus OrderStatus { get; set; }
        [Required]
        [MaxLength(200)]
        public string ShippingAddress { get; set; }
        [Required]
        [MaxLength(20)]
        public string Postalcode { get; set; }
        [Required]
        [MaxLength(100)]
        public string RecieverName { get; set; }
        [Required]
        [MaxLength(20)]
        public string PhoneNo { get; set; }
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        [ForeignKey("BuyerID")]
        public virtual User Buyer { get; set; }
        [ForeignKey("SellerID")]
        public virtual User Seller { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductReview> ProductReviews { get; set; }
    }
}
