﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDataAccess.Models
{
    [Table("Products", Schema = "emall")]
    public class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            this.CartItems = new HashSet<CartItem>();
            this.OrderItems = new HashSet<OrderItem>();
            this.ProductReviews = new HashSet<ProductReview>();
        }
        [Key]
        public int ID { get; set; }
        [Required]

        public int CategoryID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public int SellerID { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public Unit Unit { get; set; }
        [MaxLength(200)]
        public string PictureUrl { get; set; }
        [MaxLength(4000)]
        public string Description { get; set; }
        [Required]
        public decimal Inventory { get; set; }
        [Required]
        public decimal Discount { get; set; }
        [Required]
        public decimal Rating { get; set; }
        [Required]
        public int ReviewCount { get; set; }
        [Required]
        public int SalesCount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CartItem> CartItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        [ForeignKey("CategoryID")]
        public virtual ProductCategory ProductCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductReview> ProductReviews { get; set; }
        [ForeignKey("SellerID")]
        public virtual User Seller { get; set; }
    }
}
