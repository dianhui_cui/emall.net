﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDataAccess.Models
{
    [Table("OrderItems", Schema = "emall")]
    public class OrderItem
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int OrderID { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public decimal BuyPrice { get; set; }
        [Required]
        public decimal BuyCount { get; set; }
        [Required]
        public decimal Subtotal { get; set; }
        [Required]
        public int Reviewed { get; set; }
        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }
    }
}
