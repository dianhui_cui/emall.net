﻿using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessTest
{
    public class TestProductCategoryOperator
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        public void FindTopCategories()
        {
            List<ProductCategory> topCategories = ProductCategoryOperator.Single.FindTopCategories();
            Assert.AreEqual(1, topCategories.Count);
            var childCategories = ProductCategoryOperator.Single.FindChildCategories( topCategories[0].ID);

            Assert.AreEqual(6, childCategories.Count);

        }
    }
}
