﻿using System;
using System.Collections.Generic;
using System.Text;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using NUnit.Framework;

namespace DataAccessTest
{
    public class TestUserOperator
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        public void Login()
        {
            User user = UserOperator.Single.Login("jerry", "asdf123$%^", UserType.Seller);
            Assert.AreNotEqual(null, user);
        }
    }
}
