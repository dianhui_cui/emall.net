﻿using EMallDataAccess.DataAccess;
using EMallDataAccess.Views;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessTest
{
    public class TestProductOperator
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        public void FindAll()
        {
            List<ProductView> productViews = ProductOperator.Single.FindAll();
            Assert.GreaterOrEqual(productViews.Count, 2);
            Assert.AreEqual("Ordinary Fruits", productViews[0].CategoryName);
         
        }
    }
}
