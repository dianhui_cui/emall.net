﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class ProductCategoryOperator
    {
        private static ProductCategoryOperator productCategoryOperator;
        public static ProductCategoryOperator Single
        {
            get
            {
                if (productCategoryOperator == null)
                {
                    productCategoryOperator = new ProductCategoryOperator();

                }
                return productCategoryOperator;
            }
        }
        public void Add(DbProductCategory category)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.ProductCategories.Add(category);
                dbContext.SaveChanges();
            }
        }
        public void Update(DbProductCategory category)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbProductCategory old = dbContext.ProductCategories.Find(category.ID);
                old = category;
                dbContext.SaveChanges();
            }
        }
        public DbProductCategory Find(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.ProductCategories.Find(id);
            }
        }
        public List<DbProductCategory> FindTopCategories()
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<DbProductCategory> productCategories = new List<DbProductCategory>();
                try
                {

                    productCategories = dbContext.ProductCategories.Where(c => c.ParentCategory == null).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    productCategories = new List<DbProductCategory>();
                }

                return productCategories;
            }
        }
        public List<DbProductCategory> FindChildCategories(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<DbProductCategory> productCategories = new List<DbProductCategory>();
                try
                {
                    productCategories = dbContext.ProductCategories.Where(pc => pc.ParentID == id).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    productCategories = new List<DbProductCategory>();
                }

                return productCategories;
            }
        }
    }
}
