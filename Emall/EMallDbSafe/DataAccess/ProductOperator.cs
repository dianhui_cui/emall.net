﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class ProductOperator
    {
        private static ProductOperator productOperator;
        public static ProductOperator Single
        {
            get
            {
                if (productOperator == null)
                {
                    productOperator = new ProductOperator();

                }
                return productOperator;
            }
        }
        public void Add(DbProduct product)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.Products.Add(product);
                dbContext.SaveChanges();
            }
        }
        public void Update(DbProduct product)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbProduct old = dbContext.Products.Find(product.ID);
                old = product;

                dbContext.SaveChanges();
            }
        }
        public DbProduct Find(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbProduct product = dbContext.Products.Find(id);
                return product;
            }
        }
        public int FindCountAll()
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.Products.Count();
            }
        }
        public List<ProductView> FindAll()
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindAll(int pageCount,int skipCount)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Take(pageCount).Skip(skipCount).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public int FindCountByCategory(int categoryID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.Products.Where(p => p.CategoryID == categoryID).Count();
            }
        }
        public List<ProductView> FindByCategory(int categoryID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {

                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.CategoryID == categoryID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindByCategory(int categoryID,int pageCount, int skipCount)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.CategoryID == categoryID).Take(pageCount).Skip(skipCount).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }

        public int FindCountBySeller(int sellerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.Products.Where(p => p.SellerID == sellerID).Count();
            }
        }
        public List<ProductView> FindSeller(int sellerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.SellerID == sellerID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindSeller(int sellerID,int pageCount, int skipCount)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.SellerID == sellerID).Take(pageCount).Skip(skipCount).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
        public List<ProductView> FindByName(string name)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductView> products = new List<ProductView>();
                try
                {
                    var datas = dbContext.Products.Where(p => p.Name.Contains(name)).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductView() { Product = item, SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName), CategoryName = item.ProductCategory.Name });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductView>();
                }
                return products;
            }
        }
    }
}
