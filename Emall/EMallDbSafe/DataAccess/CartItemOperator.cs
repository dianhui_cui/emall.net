﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using EMallDbSafe.DataBase;
using EMallDbSafe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class CartItemOperator
    {
        private static CartItemOperator productReviewOperator;
        public static CartItemOperator Single
        {
            get
            {
                if (productReviewOperator == null)
                {
                    productReviewOperator = new CartItemOperator();

                }
                return productReviewOperator;
            }
        }
        public void Add(CartItem item)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem dbCartItem = new DbCartItem() { 
                    BuyerID=item.BuyerID,
                    CreationTS=item.CreationTS,
                    ProductID=item.ProductID,
                    Quantity=item.Quantity,
                    SellerID=item.SellerID,
                    SessionID=item.SessionID,
                    Status=item.Status
                };
                dbContext.CartItems.Add(dbCartItem);
                dbContext.SaveChanges();
            }
        }
        public void Add(int buyerID, int productID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    DbProduct product = dbContext.Products.Find(productID);
                    cartItem = new DbCartItem()
                    {
                        BuyerID = buyerID,
                        ProductID = productID,
                        Quantity = 1,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = string.Empty,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += 1;
                }
                dbContext.SaveChanges();
            }
        }
        public void Add(int buyerID, int productID,decimal quantity)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem = dbContext.CartItems.FirstOrDefault(ci => ci.BuyerID == buyerID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    DbProduct product = dbContext.Products.Find(productID);
                    cartItem = new DbCartItem()
                    {
                        BuyerID = buyerID,
                        ProductID = productID,
                        Quantity = quantity,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = string.Empty,
                        Status = 1
                    };
                    dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += quantity;
                }
                dbContext.SaveChanges();
            }
        }

        public void Add(string sessionID, int productID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem =dbContext.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    DbProduct product =dbContext.Products.Find(productID);
                    cartItem = new DbCartItem()
                    {

                        ProductID = productID,
                        Quantity = 1,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = sessionID,
                        Status = 1
                    };
                   dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += 1;
                }
               dbContext.SaveChanges();
            }
        }
        public void Add(string sessionID, int productID, decimal quantity)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem =dbContext.CartItems.FirstOrDefault(ci => ci.SessionID == sessionID && ci.ProductID == productID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {
                    DbProduct product =dbContext.Products.Find(productID);
                    cartItem = new DbCartItem()
                    {

                        ProductID = productID,
                        Quantity = quantity,
                        SellerID = product.SellerID,
                        CreationTS = DateTime.Now,
                        SessionID = sessionID,
                        Status = 1
                    };
                   dbContext.CartItems.Add(cartItem);
                }
                else
                {
                    cartItem.Quantity += quantity;
                }
               dbContext.SaveChanges();
            }
        }

        public void Update(int ciID, decimal quantity)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem =dbContext.CartItems.FirstOrDefault(ci => ci.ID == ciID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {

                }
                else
                {
                    cartItem.Quantity = quantity;
                }
               dbContext.SaveChanges();
            }
        }
        public void Update(int ciID, int status)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbCartItem cartItem = null;
                try
                {
                    cartItem =dbContext.CartItems.FirstOrDefault(ci => ci.ID == ciID);
                }
                catch (ArgumentNullException ex)
                {
                    cartItem = null;
                }

                if (null == cartItem)
                {

                }
                else
                {
                    cartItem.Status = (byte)status;
                }
               dbContext.SaveChanges();
            }
        }

        public void UpdateStore(int stID, int buyerID, int status)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {

                List<DbCartItem> list = new List<DbCartItem>();

                try
                {
                    list =dbContext.CartItems.Where(ci => ci.SellerID == stID && ci.BuyerID == buyerID).ToList();
                }
                catch (ArgumentNullException ex)
                {

                }

                if (null == list)
                {

                }
                else
                {
                    foreach (var ci in list)
                    {
                        ci.Status = (byte)status;
                    }
                }
               dbContext.SaveChanges();
            }
        }
        public void UpdateAll(int buyerID, int status)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {

                List<DbCartItem> list = new List<DbCartItem>();

                try
                {
                    list =dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                }
                catch (ArgumentNullException ex)
                {

                }

                if (null == list)
                {

                }
                else
                {
                    foreach (var ci in list)
                    {
                        ci.Status = (byte)status;
                    }
                }
               dbContext.SaveChanges();
            }
        }
        public void TransferCartItems(string sessionID, int buyerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<DbCartItem> cartItems = new List<DbCartItem>();
                try
                {
                    cartItems =dbContext.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<DbCartItem>();
                }
                foreach (var item in cartItems)
                {
                   dbContext.CartItems.Remove(item);
                }
                foreach (var item in cartItems)
                {

                    Add(buyerID, item.ProductID, item.Quantity);
                }
            }
        }

        public List<CartItemView> FindByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas =dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }
                return cartItems;
            }
        }
        public int FindQtyByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas =dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }

                return cartItems.Count();
            }
        }

        public string FindSubtotalByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                decimal subtotal = 0;
                try
                {
                    var datas =dbContext.CartItems.Where(ci => ci.BuyerID == buyerID).ToList();
                    foreach (DbCartItem ci in datas)
                    {
                        DbProduct p = ProductOperator.Single.Find(ci.ProductID);
                        subtotal += ci.Quantity * p.UnitPrice;
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                    subtotal = 0;
                }

                return string.Format("{0:0.00}", subtotal);
            }
        }
        public List<CartItemView> FindBySession(string  sessionID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<CartItemView> cartItems = new List<CartItemView>();
                try
                {
                    var datas =dbContext.CartItems.Where(ci => ci.SessionID == sessionID).ToList();
                    foreach (var item in datas)
                    {
                        cartItems.Add(new CartItemView()
                        {
                            CartItem = item,
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName),
                            ProductName = item.Product.Name,
                            BuyerName = sessionID
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    cartItems = new List<CartItemView>();
                }
                return cartItems;
            }
        }
    }
}
