﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class ProductReviewOperator
    {
        private static ProductReviewOperator productReviewOperator;
        public static ProductReviewOperator Single
        {
            get
            {
                if (productReviewOperator == null)
                {
                    productReviewOperator = new ProductReviewOperator();

                }
                return productReviewOperator;
            }
        }
        public void Add(DbProductReview productReview)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.ProductReviews.Add(productReview);
                dbContext.SaveChanges();
            }
        }
        public void Update(DbProductReview review)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbProductReview oldReview = dbContext.ProductReviews.Find(review.ID);
                oldReview = review;
                dbContext.SaveChanges();
            }
        }
        public DbProductReview Find(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.ProductReviews.Find(id);
            }
        }
        public List<ProductReviewView> FindByProduct(int productID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<ProductReviewView> products = new List<ProductReviewView>();
                try
                {
                    var datas = dbContext.ProductReviews.Where(pr => pr.ProductID == productID).ToList();
                    foreach (var item in datas)
                    {
                        products.Add(new ProductReviewView()
                        {
                            ProductReview = item,
                            ProductName = item.Product.Name,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName)
                        });
                    }

                }
                catch (ArgumentNullException ex)
                {
                    products = new List<ProductReviewView>();
                }
                return products;
            }
        }

    }
}
