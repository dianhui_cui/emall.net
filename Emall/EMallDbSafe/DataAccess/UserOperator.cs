﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class UserOperator
    {
        private static UserOperator userOperator;
        public static UserOperator Single
        {
            get {
                if (userOperator == null)
                {
                    userOperator = new UserOperator();

                }
                return userOperator;
            }
        }
    
        public void Add(DbUser user)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }
        }
        public void Update(DbUser user)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbUser oldUser = dbContext.Users.Find(user.ID);
                oldUser = user;
                dbContext.SaveChanges();
            }
        }
        public DbUser Find(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.Users.Find(id);
            }
        }
        public DbUser Login(string username, string password,UserType userType)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbUser user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Username == username && u.Password == password && u.UserType == userType);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user;
            }
        }
        public bool IsUsernameTaken(string username, int id=0)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbUser user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Username == username && u.ID != id);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user != null;
            }
        }
        public bool IsEmailTaken(string email, int id = 0)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbUser user = null;
                try
                {
                    user = dbContext.Users.FirstOrDefault(u => u.Email == email && u.ID != id);
                }
                catch (ArgumentNullException ex)
                {
                    user = null;
                }
                return user != null;
            }
        }



        }

}
