﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDbSafe.DataAccess
{
    public class OrderOperator
    {
        private static OrderOperator orderOperator;
        public static OrderOperator Single
        {
            get
            {
                if (orderOperator == null)
                {
                    orderOperator = new OrderOperator();

                }
                return orderOperator;
            }
        }
        public void Add(DbOrder order)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.Orders.Add(order);
                dbContext.SaveChanges();
            }
        }
        public void AddOrderItem(DbOrderItem orderItem)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                dbContext.OrderItems.Add(orderItem);
            }
        }
        public void Update(DbOrder order)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbOrder old = dbContext.Orders.Find(order.ID);
                old = order;
                dbContext.SaveChanges();
            }
        }
       
        public void UpdateOrderItem(DbOrderItem orderItem)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                DbOrderItem old = dbContext.OrderItems.Find(orderItem.ID);
                old = orderItem;
                dbContext.SaveChanges();
            }
        }
        public DbOrder Find(int id)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                return dbContext.Orders.Find(id);
            }
        }
        public List<OrderView> FindAllBySeller(int sellerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.SellerID == sellerID).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindAllBySeller(int sellerID,OrderStatus orderStatus)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.SellerID == sellerID && o.OrderStatus == orderStatus).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }

        public List<OrderView> FindAllBySeller(int sellerID, List<OrderStatus> orderStatus)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.SellerID == sellerID && orderStatus.Contains(o.OrderStatus)).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindBySeller(int sellerID, DateTime startTime, DateTime endTime)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.SellerID == sellerID && o.OrderTime >= startTime && o.OrderTime <= endTime).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindBySeller(int sellerID, DateTime startTime, DateTime endTime,List<OrderStatus> orderStatuses)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.SellerID == sellerID && orderStatuses.Contains(o.OrderStatus) && o.OrderTime >= startTime && o.OrderTime <= endTime).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindAllByBuyer(int buyerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.BuyerID == buyerID).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindAllByBuyer(int buyerID, OrderStatus orderStatus)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.BuyerID == buyerID && o.OrderStatus == orderStatus).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }
        public List<OrderView> FindAllByBuyer(int buyerID, List<OrderStatus> orderStatus)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderView> orders = new List<OrderView>();
                try
                {
                    var datas = dbContext.Orders.Where(o => o.BuyerID == buyerID && orderStatus.Contains(o.OrderStatus)).OrderByDescending(o => o.OrderTime).ToList();
                    foreach (var item in datas)
                    {
                        orders.Add(new OrderView()
                        {
                            Order = item,
                            BuyerName = string.IsNullOrWhiteSpace(item.Buyer.FirstName) ? item.Buyer.Username : string.Format("{0} {1}", item.Buyer.FirstName, item.Buyer.LastName),
                            SellerName = string.IsNullOrWhiteSpace(item.Seller.FirstName) ? item.Seller.Username : string.Format("{0} {1}", item.Seller.FirstName, item.Seller.LastName)
                        });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    orders = new List<OrderView>();
                }
                return orders;
            }
        }

        public List<OrderStatisticView> FindStatisticViews(int sellerID)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                List<OrderStatisticView> views = new List<OrderStatisticView>();
                string title = "", period = "";
                List<OrderStatus> statuses = new List<OrderStatus>();
                DateTime? startTime = null, endTime = null;

                title = "Orders Placed";
                period = "placed";
                startTime = null;
                endTime = null;
                statuses = new List<OrderStatus>() { OrderStatus.Placed };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));

                title = "Orders Paid";
                period = "paid";
                startTime = null;
                endTime = null;
                statuses = new List<OrderStatus>() { OrderStatus.Paid };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));

                title = "Orders Shipped";
                period = "shipped";
                startTime = null;
                endTime = null;
                statuses = new List<OrderStatus>() { OrderStatus.Shipped };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));


                title = "Orders Today";
                period = "today";
                DateTime now = DateTime.Now;
                startTime = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 0:00:00");
                endTime = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 23:59:59");
                statuses = new List<OrderStatus>() { OrderStatus.Placed, OrderStatus.Paid, OrderStatus.Shipped, OrderStatus.Delivered, OrderStatus.Returned };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));


                title = "Orders This Month";
                period = "month";
                startTime = DateTime.Parse(now.ToString("yyyy-MM") + "-01 0:00:00");
                endTime = DateTime.Parse(startTime.Value.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + " 23:59:59");
                //statuses = new List<OrderStatus>() { OrderStatus.Placed, OrderStatus.Paid, OrderStatus.Shipped, OrderStatus.Delivered, OrderStatus.Returned };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));

                title = "Orders This Quarter";
                period = "quarter";
                switch (now.Month)
                {
                    case 1:
                    case 2:
                    case 3:
                        startTime = DateTime.Parse(now.Year.ToString() + "-01-01 0:00:00");
                        endTime = DateTime.Parse(now.Year.ToString() + "-03-31 23:59:59");
                        break;
                    case 4:
                    case 5:
                    case 6:
                        startTime = DateTime.Parse(now.Year.ToString() + "-04-01 0:00:00");
                        endTime = DateTime.Parse(now.Year.ToString() + "-06-30 23:59:59");
                        break;
                    case 7:
                    case 8:
                    case 9:
                        startTime = DateTime.Parse(now.Year.ToString() + "-07-01 0:00:00");
                        endTime = DateTime.Parse(now.Year.ToString() + "-09-30 23:59:59");
                        break;
                    case 10:
                    case 11:
                    case 12:
                        startTime = DateTime.Parse(now.Year.ToString() + "-10-01 0:00:00");
                        endTime = DateTime.Parse(now.Year.ToString() + "-12-31 23:59:59");
                        break;
                }

                //statuses = new List<OrderStatus>() { OrderStatus.Placed, OrderStatus.Paid, OrderStatus.Shipped, OrderStatus.Delivered, OrderStatus.Returned };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));


                title = "Orders This Year";
                period = "year";
                startTime = DateTime.Parse(now.Year.ToString() + "-01-01 0:00:00");
                endTime = DateTime.Parse(now.Year.ToString() + "-12-31 23:59:59");
                //statuses = new List<OrderStatus>() { OrderStatus.Placed, OrderStatus.Paid, OrderStatus.Shipped, OrderStatus.Delivered, OrderStatus.Returned };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));

                title = "Orders Total";
                period = "total";
                startTime = null;
                endTime = null;
                //statuses = new List<OrderStatus>() { OrderStatus.Placed, OrderStatus.Paid, OrderStatus.Shipped, OrderStatus.Delivered, OrderStatus.Returned };
                views.Add(FindOrderStatisticView(sellerID, title, period, startTime, endTime, statuses));

                return views;
            }

        }
        public OrderStatisticView FindOrderStatisticView(int sellerID, string title,string period, DateTime? startTime, DateTime? endTime,List<OrderStatus> statuses)
        {
            using (var dbContext = EMallDbContextFactory.Single.CreateDbContext(null))
            {
                int orderCount = 0;
                decimal productCount = 0;
                decimal total = 0;
                try
                {

                    List<DbOrder> orders = new List<DbOrder>();
                    if (startTime.HasValue && endTime.HasValue)
                    {
                        orders = dbContext.Orders.Where(o => o.SellerID == sellerID && o.OrderTime >= startTime.Value && o.OrderTime <= endTime && statuses.Contains(o.OrderStatus)).ToList();

                    }
                    else
                    {
                        orders = dbContext.Orders.Where(o => o.SellerID == sellerID && statuses.Contains(o.OrderStatus)).ToList();
                    }
                    orderCount = orders.Count;
                    foreach (var order in orders)
                    {
                        total += order.Total;
                        foreach (var item in order.OrderItems)
                        {
                            productCount += item.BuyCount;
                        }
                    }
                }
                catch (ArgumentNullException ex)
                {

                }
                OrderStatisticView view = new OrderStatisticView()
                {
                    OrderCount = orderCount,
                    ProductCount = productCount,
                    Total = total,
                    Title = title,
                    PeriodOrStatus = period
                };
                return view;
            }
        }

       

    }
}
