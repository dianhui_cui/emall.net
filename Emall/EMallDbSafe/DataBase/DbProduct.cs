﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using EMallDbSafe.Common;

namespace EMallDbSafe.DataBase
{
    [Table("Products", Schema = "emall")]
    public class DbProduct
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DbProduct()
        {
            this.CartItems = new HashSet<DbCartItem>();
            this.OrderItems = new HashSet<DbOrderItem>();
            this.ProductReviews = new HashSet<DbProductReview>();
        }
        [Key]
        public int ID { get; set; }
        [Required]

        public int CategoryID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public int SellerID { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public Unit Unit { get; set; }
        [MaxLength(200)]
        public string PictureUrl { get; set; }
        [MaxLength(4000)]
        public string Description { get; set; }
        [Required]
        public decimal Inventory { get; set; }
        [Required]
        public decimal Discount { get; set; }
        [Required]
        public decimal Rating { get; set; }
        [Required]
        public int ReviewCount { get; set; }
        [Required]
        public int SalesCount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbCartItem> CartItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbOrderItem> OrderItems { get; set; }
        [ForeignKey("CategoryID")]
        public virtual DbProductCategory ProductCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProductReview> ProductReviews { get; set; }
        [ForeignKey("SellerID")]
        public virtual DbUser Seller { get; set; }
    }
}
