﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.DataBase
{
    [Table("ProductCategories", Schema = "emall")]
    public class DbProductCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DbProductCategory()
        {
            this.ChildProductCategories = new HashSet<DbProductCategory>();
            this.Products = new HashSet<DbProduct>();
        }
        [Key]
        public int ID { get; set; }
        public Nullable<int> ParentID { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProductCategory> ChildProductCategories { get; set; }
        [ForeignKey("ParentID")]
        public virtual DbProductCategory ParentCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProduct> Products { get; set; }
    }
}
