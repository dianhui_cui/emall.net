﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using EMallDbSafe.Common;

namespace EMallDbSafe.DataBase
{
    [Table("Orders", Schema = "emall")]
    public class DbOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DbOrder()
        {
            this.OrderItems = new HashSet<DbOrderItem>();
            this.ProductReviews = new HashSet<DbProductReview>();
        }
        [Key]
        public int ID { get; set; }
        [Required]
        public int BuyerID { get; set; }
        [Required]
        public int SellerID { get; set; }
        [Required]
        public System.DateTime OrderTime { get; set; }
        [Required]
        public decimal TotalBeforeTax { get; set; }
        [Required]
        public decimal ShippingBeforeTax { get; set; }
        [Required]
        public decimal Taxes { get; set; }
        [Required]
        public decimal Total { get; set; }
        [Required]
         public OrderStatus OrderStatus { get; set; }
        [Required]
        [MaxLength(200)]
        public string ShippingAddress { get; set; }
        [Required]
        [MaxLength(20)]
        public string Postalcode { get; set; }
        [Required]
        [MaxLength(100)]
        public string RecieverName { get; set; }
        [Required]
        [MaxLength(20)]
        public string PhoneNo { get; set; }
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbOrderItem> OrderItems { get; set; }
        [ForeignKey("BuyerID")]
        public virtual DbUser Buyer { get; set; }
        [ForeignKey("SellerID")]
        public virtual DbUser Seller { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProductReview> ProductReviews { get; set; }
    }
}
