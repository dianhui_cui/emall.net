﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.DataBase
{
    [Table("OrderItems", Schema = "emall")]
    public class DbOrderItem
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int OrderID { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public decimal BuyPrice { get; set; }
        [Required]
        public decimal BuyCount { get; set; }
        [Required]
        public decimal Subtotal { get; set; }
        [ForeignKey("OrderID")]
        public virtual DbOrder Order { get; set; }
        [ForeignKey("ProductID")]
        public virtual DbProduct Product { get; set; }
    }
}
