﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.DataBase
{
    public class EMallDbContext : DbContext
    {

        public EMallDbContext(DbContextOptions options) : base(options) { 
           
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
         .UseLazyLoadingProxies()
         .UseSqlServer(DbHelper.GetConnectionString());
        }
        public DbSet<DbUser> Users { get; set; }
        public DbSet<DbOrder> Orders { get; set; }
        public DbSet<DbOrderItem> OrderItems { get; set; }
        public DbSet<DbProduct> Products { get; set; }
        public DbSet<DbProductReview> ProductReviews { get; set; }
        public DbSet<DbProductCategory> ProductCategories { get; set; }
        public DbSet<DbCartItem> CartItems { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbCartItem>().HasOne(c => c.Buyer).WithMany(u => u.BuyerCartItems);
            modelBuilder.Entity<DbCartItem>().HasOne(c => c.Seller).WithMany(u => u.SellerCartItems);
            modelBuilder.Entity<DbCartItem>().HasOne(c => c.Product).WithMany(p=>p.CartItems);
            modelBuilder.Entity<DbOrder>().HasOne(o => o.Buyer).WithMany(u => u.BuyerOrders);
            modelBuilder.Entity<DbOrder>().HasOne(o => o.Seller).WithMany(u => u.SellerOrders);
            modelBuilder.Entity<DbOrderItem>().HasOne(oi => oi.Order).WithMany(o => o.OrderItems);
            modelBuilder.Entity<DbOrderItem>().HasOne(oi => oi.Product).WithMany(p => p.OrderItems);
            modelBuilder.Entity<DbProduct>().HasOne(p => p.ProductCategory).WithMany(pc => pc.Products);
            modelBuilder.Entity<DbProduct>().HasOne(p => p.Seller).WithMany(u => u.Products);
            modelBuilder.Entity<DbProductCategory>().HasOne(pc => pc.ParentCategory).WithMany(pc => pc.ChildProductCategories);
            modelBuilder.Entity<DbProductReview>().HasOne(pr => pr.Buyer).WithMany(u => u.ProductReviews);
            modelBuilder.Entity<DbProductReview>().HasOne(pr => pr.Product).WithMany(p=>p.ProductReviews);
            modelBuilder.Entity<DbProductReview>().HasOne(pr => pr.Order).WithMany(o => o.ProductReviews);
        }
    }
}
