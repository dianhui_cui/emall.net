﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Common
{
    public enum UserType
    { 
        Buyer,
        Seller,
        Admin
    }
    public enum Unit
    {
        ea,
        lb,
        kg,
        box
    }
    public enum OrderStatus
    { 
        Placed,Paid,Shipped,Delivered,Returned
    }
}
