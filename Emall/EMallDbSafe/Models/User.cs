﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.Models
{
   
    public class User
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DbUser()
        {
            this.BuyerCartItems = new HashSet<DbCartItem>();
            this.SellerCartItems = new HashSet<DbCartItem>();
            this.BuyerOrders = new HashSet<DbOrder>();
            this.SellerOrders = new HashSet<DbOrder>();
            this.ProductReviews = new HashSet<DbProductReview>();
            this.Products = new HashSet<DbProduct>();
        }
        [Key]
        public int ID { get; set; }
        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar(50)")]
        public string Username { get; set; }
        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar(100)")]
        public string Password { get; set; }
        [Required]
        [MaxLength(100)]
        [Column(TypeName = "varchar(100)")]
        public string Email { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(10)]
        public string MiddleName { get; set; }
        [Required]
        public System.DateTime RegisterTime { get; set; }
        [Required]
        public int OrderCount { get; set; }
        [Required]
        public double Score { get; set; }
        [MaxLength(50)]
        public string Address { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(20)]
        public string Province { get; set; }
        [MaxLength(20)]
        public string Country { get; set; }
        [MaxLength(20)]
        public string PostalCode { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        [MaxLength(2000)]
        public string ShopPage { get; set; }
        [Required]
        public UserType UserType { get; set; }
        [MaxLength(20)]
        public string PhoneNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbCartItem> BuyerCartItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbCartItem> SellerCartItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbOrder> BuyerOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbOrder> SellerOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProductReview> ProductReviews { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProduct> Products { get; set; }

    }
}
