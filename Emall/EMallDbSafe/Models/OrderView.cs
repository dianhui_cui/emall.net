﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Views
{
    public class OrderView
    {
        public DbOrder Order { get; set; }
        public string SellerName { get; set; }
        public string BuyerName { get; set; }

    }
}
