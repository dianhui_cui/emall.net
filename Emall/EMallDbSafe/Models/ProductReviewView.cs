﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Views
{
    public class ProductReviewView
    {
        public DbProductReview ProductReview { get; set; }
        public string ProductName { get; set; }
        public string BuyerName { get; set; }
    }
}
