﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Views
{
    public class ProductView
    {
        public DbProduct Product { get; set; }
        public string SellerName { get; set; }
        public string CategoryName { get; set; }
    }
}
