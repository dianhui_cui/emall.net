﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.Models
{
    [Table("ProductReviews", Schema = "emall")]
    public class DbProductReview
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int ProductID { get; set; }
        [MaxLength(2000)]
        public string Review { get; set; }
        [Required]
        public decimal Rating { get; set; }
        [Required]
        public System.DateTime CreationTime { get; set; }
        [Required]
        public int OrderID { get; set; }
        [Required]
        public int BuyerID { get; set; }
        [ForeignKey("OrderID")]
        public virtual DbOrder Order { get; set; }
        [ForeignKey("ProductID")]
        public virtual DbProduct Product { get; set; }
        [ForeignKey("BuyerID")]
        public virtual DbUser Buyer { get; set; }
    }
}
