﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Views
{
    public class OrderStatisticView
    {
        public string Title { get; set; }
        public string PeriodOrStatus { get; set; }
        public int OrderCount { get; set; }
        public decimal ProductCount { get; set; }
        public decimal Total { get; set; }
    }
}
