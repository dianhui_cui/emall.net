﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDbSafe.Models
{
    public class CartItemView
    {
        public CartItem CartItem { get; set; }
        public string ProductName { get; set; }
        public string SellerName { get; set; }
        public string BuyerName { get; set; }
    }
}
