﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.Models
{
   
    public class Order
    {
       
        public Order()
        {
            this.OrderItems = new HashSet<DbOrderItem>();
            this.ProductReviews = new HashSet<DbProductReview>();
        }
     
        public int ID { get; set; }
      
        public int BuyerID { get; set; }
        
        public int SellerID { get; set; }
       
        public System.DateTime OrderTime { get; set; }
      
        public decimal TotalBeforeTax { get; set; }
        
        public decimal ShippingBeforeTax { get; set; }
       
        public decimal Taxes { get; set; }
        
        public decimal Total { get; set; }
      
         public OrderStatus OrderStatus { get; set; }
       
        public string ShippingAddress { get; set; }
      
        public string Postalcode { get; set; }
       
        public string RecieverName { get; set; }
    
        public string PhoneNo { get; set; }
     
        public string Email { get; set; }

       
        public virtual ICollection<DbOrderItem> OrderItems { get; set; }
        [ForeignKey("BuyerID")]
        public virtual DbUser Buyer { get; set; }
        [ForeignKey("SellerID")]
        public virtual DbUser Seller { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DbProductReview> ProductReviews { get; set; }
    }
}
