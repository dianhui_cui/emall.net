﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EMallDbSafe.Models
{
  
    public class CartItem
    {
      
        public int ID { get; set; }
  
        public string SessionID { get; set; }
        public Nullable<int> BuyerID { get; set; }
        public Nullable<int> SellerID { get; set; }
        public int ProductID { get; set; }
        public decimal Quantity { get; set; }
        public System.DateTime CreationTS { get; set; }
        public byte Status { get; set; }

        public virtual Product Product { get
            { 
                
            } }

        public virtual User Buyer
        {
            get; set;
        }
        public virtual User Seller
        {
            get; set;
        }
    }
}
