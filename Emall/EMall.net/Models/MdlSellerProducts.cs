﻿using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerProducts : SellerModelBase
    {
        public Dictionary<int, CategoryProducts> DicProducts;
    }
    public class CategoryProducts
    { 
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public List<ProductView> Products { get; set; }
    }
}
