﻿using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlComProductCategory
    {
        public int ID { get; set; }
        public Nullable<int> ParentID { get; set; }
        public string Name { get; set; }
        private List<MdlComProductCategory> childs;
        public List<MdlComProductCategory> ChildProductCategories
        {
            get {
                if (null == childs)
                {
                    childs = new List<MdlComProductCategory>();
                    List<ProductCategory> categories = ProductCategoryOperator.Single.FindChildCategories(this.ID);
                    foreach (var item in categories)
                    {
                        childs.Add(new MdlComProductCategory() { 
                            ID=item.ID,
                            Name=item.Name,
                            ParentID=item.ParentID
                        });
                    }
                }
                return childs;
            }
        }
     

     
    }
}
