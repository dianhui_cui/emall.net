﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerSession
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
    }
}
