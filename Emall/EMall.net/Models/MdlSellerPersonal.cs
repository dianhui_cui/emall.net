﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerPersonal : SellerModelBase
    {

       public UserBasicInfo UserBasicInfo { get; set; }
       public UserAddressInfo UserAddressInfo { get; set; }
       public UserDescriptionInfo UserDescriptionInfo { get; set; }
       public UserChangePasswordInfo UserChangePasswordInfo { get; set; }
    }
    public class UserBasicInfo
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please Enter Username")]
        [Display(Name = "Username")]
        [RegularExpression("[a-zA-Z0-9]{1,50}", ErrorMessage = "Please Enter Correct Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please Enter Email Address")]
        [Display(Name = "Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }

        [MaxLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(10)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [MaxLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [MaxLength(20)]
        [Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

    }
    public class UserAddressInfo
    {

        public int ID { get; set; }
        [MaxLength(50)]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [MaxLength(50)]
        [Display(Name = "City")]
        public string City { get; set; }
        [MaxLength(20)]
        [Display(Name = "Province")]
        public string Province { get; set; }
        [MaxLength(20)]
        [Display(Name = "Country")]
        public string Country { get; set; }
        [MaxLength(20)]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
    }
    public class UserDescriptionInfo
    {
        public int ID { get; set; }
        [MaxLength(200)]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }

    public class UserChangePasswordInfo
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please Enter Old Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "OldPassword")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
