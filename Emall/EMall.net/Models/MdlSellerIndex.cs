﻿using EMallDataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerIndex: SellerModelBase
    {
        public List<OrderStatisticView> StatisticViews { get; set; }
    }
}
