﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerProductDelete : SellerModelBase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string PictureUrl { get; set; }

        public List<String> Errors { get; set; }

    }
}
