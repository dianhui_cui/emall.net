﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerLogin
    {
        [Required(ErrorMessage = "Please Enter Username")]
        [Display(Name = "Username")]
        [RegularExpression("[a-zA-Z0-9]{1,50}", ErrorMessage = "Please Enter Correct Username")]
        public string Username { get; set; }


        [Required(ErrorMessage = "Please Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
