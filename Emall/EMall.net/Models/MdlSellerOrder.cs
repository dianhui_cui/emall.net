﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerOrder
    {

        public int ID { get; set; }
        public int BuyerID { get; set; }
        public int SellerID { get; set; }
        public DateTime OrderTime { get; set; }
        public decimal TotalBeforeTax { get; set; }
       
        public decimal ShippingBeforeTax { get; set; }
       
        public decimal Taxes { get; set; }
       
        public decimal Total { get; set; }
       
        public int OrderStatus { get; set; }
       
      
        public string ShippingAddress { get; set; }
       
       
        public string Postalcode { get; set; }
       
       
        public string RecieverName { get; set; }
       
       
        public string PhoneNo { get; set; }
       
      
        public string Email { get; set; }

        public string SellerName { get; set; }
        public string BuyerName { get; set; }
    }
}
