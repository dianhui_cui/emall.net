﻿using EMallDataAccess.Models;
using EMallDataAccess.Views;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EMall.net.Models
{
    public class MdlSellerProductAddUpdate : SellerModelBase
    {
     
        public List<MdlComProductCategory> Categories { get; set; }
        public int ID { get; set; }
        public int SellerID { get; set; }

        [Required(ErrorMessage = "Please select category")]
        [Display(Name = "Category")]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "Please Enter Product Name")]
        [Display(Name = "Name")]
        [RegularExpression("[a-zA-Z0-9]{1,50}", ErrorMessage = "Please enter Correct name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Unit Price")]
        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }
        [Required(ErrorMessage = "Please select Unit Price")]
        [Display(Name = "Unit")]
        public Unit Unit { get; set; }

        [Required(ErrorMessage = "Please Enter Inventory")]
        [Display(Name = "Inventory")]
        public decimal Inventory { get; set; }

        [Required(ErrorMessage = "Please Enter Discount")]
        [Display(Name = "Discount")]
        public decimal Discount { get; set; }

        [Display(Name = "Picture")]
        public string PictureUrl { get; set; }

        [Display(Name = "Upload Picture")]
        public IFormFile Picture { get; set; }

      
       
        [Display(Name = "Description")]
        [RegularExpression("[\\s\\S]{0,4000}", ErrorMessage = "Please enter Correct Description")]
        public string Description { get; set; }

        [Display(Name = "Rating")]
        public decimal Rating { get; set; }
        [Display(Name = "ReviewCount")]
        public int ReviewCount { get; set; }
        [Display(Name = "SalesCount")]
        public int SalesCount { get; set; }


    }
}
