﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EMall.net.Models;
using EMallDataAccess.DataAccess;
using EMallDataAccess.Models;
using EMallDataAccess.Views;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EMall.net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestController : ControllerBase
    {
        [HttpGet("checkusername")]
        public bool CheckUsername(string username, int id)
        {
            return  UserOperator.Single.IsUsernameTaken(username);
        }

        [HttpGet("checkemail")]
        public bool CheckEmail(string email, int id)
        {
            return UserOperator.Single.IsEmailTaken(email);
        }
      
        [HttpPut("changeuserbasicinfo/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
      
        public object ChangeUserBasicInfo(int id)
        {
           
            string content = Request.Form["item"];
            
            
            List<string> errors = new List<string>();
            UserBasicInfo basicInfo = JsonConvert.DeserializeObject<UserBasicInfo>(content);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (seller == null || seller.ID != basicInfo.ID)
            {
                errors.Add("Operate forbidden.");
            }
            if (basicInfo.FirstName.Length>50)
            {
                errors.Add("First name must be 0-50 charactors.");
            }
            if (basicInfo.LastName.Length > 50)
            {
                errors.Add("Last name must be 0-50 charactors.");
            }
            if (basicInfo.MiddleName.Length > 10)
            {
                errors.Add("Middle name must be 0-10 charactors.");
            }
            if (basicInfo.PhoneNo.Length > 20)
            {
                errors.Add("Phone Number name must be 0-20 charactors.");
            }
            if (errors.Count>0)
            {
               return JsonConvert.SerializeObject(errors);
            }
            else
            {
                EMallDataAccess.Models.User user = UserOperator.Single.Find(basicInfo.ID);
                user.FirstName = basicInfo.FirstName;
                user.MiddleName = basicInfo.MiddleName;
                user.LastName = basicInfo.LastName;
                user.PhoneNo = basicInfo.PhoneNo;
                UserOperator.Single.Update(user);
                return JsonConvert.SerializeObject(true);
            }
            
        }

     
        [HttpPut("changeuseraddressinfo/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public object ChangeUserAddressInfo(int id)
        {
            string content = Request.Form["item"];
            List<string> errors = new List<string>();
            UserAddressInfo addressInfo = JsonConvert.DeserializeObject<UserAddressInfo>(content);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (seller == null || seller.ID != addressInfo.ID)
            {
                errors.Add("Operate forbidden.");
            }
            if (addressInfo.Address.Length > 50)
            {
                errors.Add("Address must be 0-50 charactors.");
            }
            if (addressInfo.City.Length > 50)
            {
                errors.Add("City must be 0-50 charactors.");
            }
            if (addressInfo.Province.Length > 20)
            {
                errors.Add("Province must be 0-20 charactors.");
            }
            if (addressInfo.Country.Length > 20)
            {
                errors.Add("Country must be 0-20 charactors.");
            }
            if (addressInfo.PostalCode.Length > 20)
            {
                errors.Add("PostalCode name must be 0-20 charactors.");
            }
            if (errors.Count > 0)
            {
                return JsonConvert.SerializeObject(errors);
            }
            else
            {
                EMallDataAccess.Models.User user = UserOperator.Single.Find(addressInfo.ID);
                user.Address = addressInfo.Address;
                user.City = addressInfo.City;
                user.Province = addressInfo.Province;
                user.Country = addressInfo.Country;
                user.PostalCode = addressInfo.PostalCode;
                UserOperator.Single.Update(user);
                return JsonConvert.SerializeObject(true);
            }
        }

      
        [HttpPut("changeuserdescriptioninfo/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public object ChangeUserDescriptionInfo(int id)
        {
            string content = Request.Form["item"];
            List<string> errors = new List<string>();
            UserDescriptionInfo descriptionInfo = JsonConvert.DeserializeObject<UserDescriptionInfo>(content);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (seller == null || seller.ID != descriptionInfo.ID)
            {
                errors.Add("Operate forbidden.");
            }
            if (descriptionInfo.Description.Length > 200)
            {
                errors.Add("Address must be 0-200 charactors.");
            }
   
            if (errors.Count > 0)
            {
                return JsonConvert.SerializeObject(errors);
            }
            else
            {
                EMallDataAccess.Models.User user = UserOperator.Single.Find(descriptionInfo.ID);
                user.Description = descriptionInfo.Description;
                UserOperator.Single.Update(user);
                return JsonConvert.SerializeObject(true);
            }
        }

     
        [HttpPut("changeuserpasswordinfo/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public object ChangeUserPasswordInfo(int id)
        {
            string content = Request.Form["item"];
            List<string> errors = new List<string>();
            UserChangePasswordInfo passwordInfo = JsonConvert.DeserializeObject<UserChangePasswordInfo>(content);
            EMallDataAccess.Models.User user = UserOperator.Single.Find(passwordInfo.ID);
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (seller == null || seller.ID != passwordInfo.ID)
            {
                errors.Add("Operate forbidden.");
            }
            if (null != user)
            {
                if (passwordInfo.OldPassword != user.Password)
                {
                    errors.Add("Old Password Error.");
                }
                if (passwordInfo.Password.Length<6|| passwordInfo.Password.Length > 100)
                {
                    errors.Add("Password must be 6-100 charactors.");
                }
                if (passwordInfo.Password!=passwordInfo.ConfirmPassword)
                {
                    errors.Add("Confirm password is not same to password.");
                }

                if (errors.Count > 0)
                {
                    return JsonConvert.SerializeObject(errors);
                }
                else
                {

                    user.Password = passwordInfo.Password;
                    UserOperator.Single.Update(user);
                    return JsonConvert.SerializeObject(true);
                }
            }
            else
            {
                errors.Add("Can't find user,change password failed.");
                return JsonConvert.SerializeObject(errors); 
            }
        }

        [HttpGet("productstatistic")]
        public object ProductStatistic(string period, string startTimeStr, string endTimeStr)
        {
            List<ProductSaleStatistic> datas = new List<ProductSaleStatistic>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;
                if (period == "anyTime")
                {
                    startTime = DateTime.Parse(startTimeStr);
                    endTime = DateTime.Parse(endTimeStr);
                }
                else
                {
                    GetTime(period, out startTime, out endTime);
                }
                string sql = "";
                switch (period)
                {
                    case "month":
                        sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,CONVERT(varchar, c.OrderTime, 23) AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                            "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,CONVERT(varchar, c.OrderTime, 23)"
                            , startTime.ToString("yyyy-MM-dd HH:mm:ss"),endTime.ToString("yyyy-MM-dd HH:mm:ss"),seller.ID);
                        break;
                    case "quarter":
                        sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM') AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                           "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM')"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        break;
                    case "year":
                        sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM') AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                        "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM')"
                        , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        break;
                    case "anyTime":
                        TimeSpan timeSpan = endTime - startTime;
                        double days = timeSpan.TotalDays;
                        if (days <= 31)
                        {
                            sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,CONVERT(varchar, c.OrderTime, 23) AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                           "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,CONVERT(varchar, c.OrderTime, 23)"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else if (days > 31 && days <= 366)
                        {
                            sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM') AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                           "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,FORMAT(c.OrderTime, 'MMMM')"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else {

                            sql = string.Format("select a.ProductId,b.Name as ProductName,b.PictureUrl,YEAR(c.OrderTime) AS ProductPeriod,sum(a.BuyCount) as TotalCount,sum(a.Subtotal) as TotalSales from emall.OrderItems a, emall.Products b,emall.Orders c " +
                           "where a.ProductId=b.ID and a.OrderId=c.ID and c.OrderTime>='{0}' and c.OrderTime<='{1}' and c.SellerID={2}  group by a.ProductId,b.Name,b.PictureUrl,YEAR(c.OrderTime)"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        break;
                }
                datas = StatisticOperator.Single.FindProductSaleStatistics(sql);

            }
            return datas;
        }

        [HttpGet("salestatistic")]
        public object SaleStatistic(string period, string startTimeStr, string endTimeStr)
        {
            List<OrderSaleStatistic> datas = new List<OrderSaleStatistic>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;
                if (period == "anyTime")
                {
                    startTime = DateTime.Parse(startTimeStr);
                    endTime = DateTime.Parse(endTimeStr);
                }
                else
                {
                    GetTime(period, out startTime, out endTime);
                }
                string sql = "";
                switch (period)
                {
                    case "month":
                        sql = string.Format("select CONVERT(varchar, a.OrderTime, 23) AS OrderPeriod,sum(a.Total) as SalesTotal from emall.Orders a " +
                            "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.SellerID={2} group by CONVERT(varchar, a.OrderTime, 23)"
                            , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        break;
                    case "quarter":
                    case "year":
                        sql = string.Format("select FORMAT(a.OrderTime, 'MMMM')  AS OrderPeriod,sum(a.Total) as SalesTotal from  emall.Orders a " +
                           "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.SellerID={2} group by FORMAT(a.OrderTime, 'MMMM')"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        break;
                   
                    case "anyTime":
                        TimeSpan timeSpan = endTime - startTime;
                        double days = timeSpan.TotalDays;
                        if (days <= 31)
                        {
                            sql = string.Format("select CONVERT(varchar, a.OrderTime, 23) AS OrderPeriod,sum(a.Total) as SalesTotal from  emall.Orders a " +
                           "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.SellerID={2} group by CONVERT(varchar, a.OrderTime, 23)"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else if (days > 31 && days <= 366)
                        {
                            sql = string.Format("select FORMAT(a.OrderTime, 'MMMM')  AS OrderPeriod,sum(a.Total) as SalesTotal from  emall.Orders a " +
                            "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.SellerID={2} group by FORMAT(a.OrderTime, 'MMMM')"
                            , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else
                        {

                            sql = string.Format("select YEAR(a.OrderTime)  AS OrderPeriod,sum(a.Total) as SalesTotal from  emall.Orders a " +
                            "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.SellerID={2} group by  YEAR(a.OrderTime)"
                            , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        break;
                }
                datas = StatisticOperator.Single.FindOrderSaleStatistics(sql);

            }
            return datas;
        }

        [HttpGet("customerstatistic")]
        public object CustomerStatistic(string period, string startTimeStr, string endTimeStr)
        {
            List<CustomerSaleStatistic> datas = new List<CustomerSaleStatistic>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;
                if (period == "anyTime")
                {
                    startTime = DateTime.Parse(startTimeStr);
                    endTime = DateTime.Parse(endTimeStr);
                }
                else
                {
                    GetTime(period, out startTime, out endTime);
                }
                string sql = "";
                switch (period)
                {
                    case "month":
                        sql = string.Format("select a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName) AS BuyerName, CONVERT(varchar, a.OrderTime, 23) AS OrderPeriod, sum(a.Total) as BuyTotal from  emall.Orders a,  emall.Users b " +
                            "where a.BuyerID=b.ID and a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}  group by a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName), CONVERT(varchar, a.OrderTime, 23)"
                            , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        break;
                    case "quarter":
                    case "year":
                        sql = string.Format("select a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName) AS BuyerName, FORMAT(a.OrderTime, 'MMMM') AS OrderPeriod, sum(a.Total) as BuyTotal from  emall.Orders a,  emall.Users b " +
                           "where a.BuyerID=b.ID and a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}  group by a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName), FORMAT(a.OrderTime, 'MMMM')"
                           , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                      
                        break;

                    case "anyTime":
                        TimeSpan timeSpan = endTime - startTime;
                        double days = timeSpan.TotalDays;
                        if (days <= 31)
                        {
                            sql = string.Format("select a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName) AS BuyerName, CONVERT(varchar, a.OrderTime, 23) AS OrderPeriod, sum(a.Total) as BuyTotal from  emall.Orders a,  emall.Users b " +
                              "where a.BuyerID=b.ID and a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}  group by a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName), CONVERT(varchar, a.OrderTime, 23)"
                              , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else if (days > 31 && days <= 366)
                        {
                            sql = string.Format("select a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName) AS BuyerName, FORMAT(a.OrderTime, 'MMMM') AS OrderPeriod, sum(a.Total) as BuyTotal from  emall.Orders a,  emall.Users b " +
                             "where a.BuyerID=b.ID and a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}  group by a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName), FORMAT(a.OrderTime, 'MMMM')"
                             , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        else
                        {

                            sql = string.Format("select a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName) AS BuyerName, YEAR(a.OrderTime) AS OrderPeriod, sum(a.Total) as BuyTotal from  emall.Orders a,  emall.Users b " +
                             "where a.BuyerID=b.ID and a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}  group by a.BuyerID,CONCAT_WS(' ', b.FirstName, b.LastName),YEAR(a.OrderTime)"
                             , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                        }
                        break;
                }
                datas = StatisticOperator.Single.FindCustomerSaleStatistics(sql);

            }
            return datas;
        }

        [HttpGet("orderstatistic")]
        public object OrderStatistic(string period, string startTimeStr, string endTimeStr)
        {
            List<OrderCountStatistic> orders = new List<OrderCountStatistic>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;
                if (period == "anyTime")
                {
                    startTime = DateTime.Parse(startTimeStr);
                    endTime = DateTime.Parse(endTimeStr);
                }
                else
                {
                    GetTime(period, out startTime, out endTime);
                }
                string sql= string.Format("select a.ID,FORMAT(a.OrderTime,'yyyy-MM-dd HH:mm:ss') as OrderTime from emall.Orders a " +
                             "where a.OrderTime>='{0}' and a.OrderTime<='{1}' and a.sellerId={2}"
                             , startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"), seller.ID);
                orders = StatisticOperator.Single.FindOrderCountStatistics(sql);
          
            }
            return orders;
            //return JsonConvert.SerializeObject(orders);
        }

        private void GetTime(string period, out DateTime startTime, out DateTime endTime)
        {
            DateTime now =DateTime.Now;
            startTime = DateTime.Parse(now.ToString("yyyy-MM-dd")+" 0:00:00");
            endTime = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 23:59:59");
            switch (period)
            {
                case "month":
                    startTime = DateTime.Parse(now.ToString("yyyy-MM") + "-01 0:00:00");
                    endTime = DateTime.Parse(startTime.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd")+" 23:59:59");
                    break;
                case "quarter":
                    switch (now.Month)
                    {
                        case 1:
                        case 2:
                        case 3:
                            startTime = DateTime.Parse(string.Format("{0}-01-01 0:00:00",now.Year));
                            endTime = DateTime.Parse(string.Format("{0}-03-31 23:59:59", now.Year));
                            break;
                        case 4:
                        case 5:
                        case 6:
                            startTime = DateTime.Parse(string.Format("{0}-04-01 0:00:00", now.Year));
                            endTime = DateTime.Parse(string.Format("{0}-06-30 23:59:59", now.Year));
                            break;
                        case 7:
                        case 8:
                        case 9:
                            startTime = DateTime.Parse(string.Format("{0}-07-01 0:00:00", now.Year));
                            endTime = DateTime.Parse(string.Format("{0}-09-30 23:59:59", now.Year));
                            break;
                        case 10:
                        case 11:
                        case 12:
                            startTime = DateTime.Parse(string.Format("{0}-10-01 0:00:00", now.Year));
                            endTime = DateTime.Parse(string.Format("{0}-12-31 23:59:59", now.Year));
                            break;

                    }

                    break;
                case "year":
                    startTime = DateTime.Parse(string.Format("{0}-01-01 0:00:00", now.Year));
                    endTime = DateTime.Parse(string.Format("{0}-12-31 23:59:59", now.Year));
                    break;
           
            }
        }
        [HttpGet("changeorderstatus")]
        public object ChangeOrderStatus(int orderID,int status)
        {
          
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                Order order = OrderOperator.Single.Find(orderID);
                if (null == order || order.SellerID != seller.ID) return false;
                OrderOperator.Single.ChangeOrderStatus(orderID, (OrderStatus)status);
             
                return true;
            }
            else
            {
                return false;
            }
           

        }

        [HttpGet("ordersinprocess")]
        public object OrdersInProcess(int status )
        {
            List<MdlSellerOrder> mdlOrders = new List<MdlSellerOrder>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                List<OrderView> orders = OrderOperator.Single.FindAllBySeller(seller.ID, (OrderStatus)status);
                foreach (var item in orders)
                {

                    mdlOrders.Add(new MdlSellerOrder()
                    {

                        ID = item.Order.ID,
                        BuyerID = item.Order.BuyerID,
                        BuyerName = item.BuyerName,
                        Email = item.Order.Email,
                        OrderStatus=(int)item.Order.OrderStatus,
                        OrderTime=item.Order.OrderTime,
                        PhoneNo=item.Order.PhoneNo,
                        Postalcode=item.Order.Postalcode,
                        RecieverName=item.Order.RecieverName,
                        SellerID=item.Order.SellerID,
                        SellerName=item.SellerName,
                        ShippingAddress=item.Order.ShippingAddress,
                        ShippingBeforeTax=item.Order.ShippingBeforeTax,
                        Taxes=item.Order.Taxes,
                        Total=item.Order.Total,
                        TotalBeforeTax=item.Order.TotalBeforeTax
                    }) ;
                }
            }
            return mdlOrders;
          
        }

        [HttpGet("ordershistory")]
        public object OrdersHistory(string period, string startTimeStr, string endTimeStr)
        {

       

            List<MdlSellerOrder> mdlOrders = new List<MdlSellerOrder>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;
                if (period == "anyTime")
                {
                    startTime = DateTime.Parse(startTimeStr);
                    endTime = DateTime.Parse(endTimeStr);
                }
                else
                {
                    GetTime(period, out startTime, out endTime);
                }
                List<OrderView> orders = OrderOperator.Single.FindBySeller(seller.ID, startTime, endTime);
                foreach (var item in orders)
                {

                    mdlOrders.Add(new MdlSellerOrder()
                    {

                        ID = item.Order.ID,
                        BuyerID = item.Order.BuyerID,
                        BuyerName = item.BuyerName,
                        Email = item.Order.Email,
                        OrderStatus = (int)item.Order.OrderStatus,
                        OrderTime = item.Order.OrderTime,
                        PhoneNo = item.Order.PhoneNo,
                        Postalcode = item.Order.Postalcode,
                        RecieverName = item.Order.RecieverName,
                        SellerID = item.Order.SellerID,
                        SellerName = item.SellerName,
                        ShippingAddress = item.Order.ShippingAddress,
                        ShippingBeforeTax = item.Order.ShippingBeforeTax,
                        Taxes = item.Order.Taxes,
                        Total = item.Order.Total,
                        TotalBeforeTax = item.Order.TotalBeforeTax
                    });
                }
            }
            return mdlOrders;

        }

        [HttpGet("orderitems")]
        public object OrderItems(int orderID)
        {
            List<MdlSellerOrderItem> mdlOrderItems = new List<MdlSellerOrderItem>();
            MdlSellerSession seller = HttpContext.Session.Keys.Contains("Seller") ? JsonConvert.DeserializeObject<MdlSellerSession>(HttpContext.Session.GetString("Seller")) : null;
            if (null != seller)
            {
                
                List<OrderItemView> orders = OrderOperator.Single.FindOrderItemByOrder(orderID);
                foreach (var item in orders)
                {

                    mdlOrderItems.Add(new MdlSellerOrderItem()
                    {

                        ID = item.OrderItem.ID,
                        BuyCount=item.OrderItem.BuyCount,
                        BuyPrice=item.OrderItem.BuyPrice,
                        OrderID=item.OrderItem.OrderID,
                        PictureUrl=item.PictureUrl,
                        ProductID=item.OrderItem.ProductID,
                        ProductName=item.ProductName,
                       Subtotal=item.OrderItem.Subtotal,
                       UnitPrice=item.OrderItem.UnitPrice
                        
                    });
                }
            }
            return mdlOrderItems;

        }

    }
}