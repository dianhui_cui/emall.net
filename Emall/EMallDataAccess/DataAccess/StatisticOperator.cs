﻿using EMallDataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class StatisticOperator
    {
        private static StatisticOperator productOperator;
        public static StatisticOperator Single
        {
            get
            {
                if (productOperator == null)
                {
                    productOperator = new StatisticOperator();

                }
                return productOperator;
            }
        }
        public List<CustomerSaleStatistic> FindCustomerSaleStatistics(string sql)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<CustomerSaleStatistic> statistics = dbContext.CustomerSaleStatistics.FromSql(sql).ToList();

                return statistics;
            }
        }
        public List<OrderSaleStatistic> FindOrderSaleStatistics(string sql)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<OrderSaleStatistic> statistics = dbContext.OrderSaleStatistics.FromSql(sql).ToList();

                return statistics;
            }
        }
        public List<ProductSaleStatistic> FindProductSaleStatistics(string sql)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<ProductSaleStatistic> statistics = dbContext.ProductSaleStatistics.FromSql(sql).ToList();

                return statistics;
            }
        }
        public List<OrderCountStatistic> FindOrderCountStatistics(string sql)
        {
            using (var dbContext = EMallDbContext.NewInstance)
            {
                List<OrderCountStatistic> statistics = dbContext.OrderCountStatistics.FromSql(sql).ToList();

                return statistics;
            }
        }
        #region backup Single mode
        /*
           public List<CustomerSaleStatistic> FindCustomerSaleStatistics(string sql)
        {
            List<CustomerSaleStatistic> statistics = EMallDbContext.Single.CustomerSaleStatistics.FromSql(sql).ToList();

            return statistics;
        }
        public List<OrderSaleStatistic> FindOrderSaleStatistics(string sql)
        {
            List<OrderSaleStatistic> statistics = EMallDbContext.Single.OrderSaleStatistics.FromSql(sql).ToList();

            return statistics;
        }
        public List<ProductSaleStatistic> FindProductSaleStatistics(string sql)
        {
            List<ProductSaleStatistic> statistics = EMallDbContext.Single.ProductSaleStatistics.FromSql(sql).ToList();

            return statistics;
        }
        public List<OrderCountStatistic> FindOrderCountStatistics(string sql)
        {
            List<OrderCountStatistic> statistics = EMallDbContext.Single.OrderCountStatistics.FromSql(sql).ToList();

            return statistics;
        }
         */
        #endregion
    }
}
