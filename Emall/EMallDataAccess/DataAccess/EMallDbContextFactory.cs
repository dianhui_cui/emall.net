﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.DataAccess
{
    public class EMallDbContextFactory : IDesignTimeDbContextFactory<EMallDbContext>
    {
        private static EMallDbContextFactory factory;
        private static readonly object objLock = new object();
        public static EMallDbContextFactory Single {
            get
            {
                lock (objLock)
                {
                    if (null == factory)
                    {
                        factory = new EMallDbContextFactory();
                    }
                    return factory;
                }
            }
        }
        public EMallDbContext CreateDbContext(string[] args)
        {
            DbContextOptions options = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), DbHelper.GetConnectionString()).Options;
            EMallDbContext dbContext = new EMallDbContext(options);
            return dbContext;
        }
    }
}
