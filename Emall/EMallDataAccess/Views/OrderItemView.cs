﻿using EMallDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Views
{
    public class OrderItemView
    {
        public OrderItem OrderItem { get; set; }
        public string ProductName { get; set; }
        public string PictureUrl { get; set; }

    }
}
