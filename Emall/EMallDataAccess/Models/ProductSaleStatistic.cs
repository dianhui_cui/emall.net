﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Models
{
    public class ProductSaleStatistic
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string PictureUrl { get; set; }
        public string ProductPeriod { get; set; }
        public decimal TotalCount { get; set; }
        public decimal TotalSales { get; set; }
    }
}
