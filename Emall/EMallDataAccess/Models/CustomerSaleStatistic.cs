﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Models
{
    public class CustomerSaleStatistic
    {
        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string OrderPeriod { get; set; }
        public decimal BuyTotal { get; set; }
    }
}
