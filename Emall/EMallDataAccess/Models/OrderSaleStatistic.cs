﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMallDataAccess.Models
{
    public class OrderSaleStatistic
    {
        public string OrderPeriod { get; set; }
        public decimal SalesTotal { get; set; }
    }
}
