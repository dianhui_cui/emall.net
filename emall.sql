CREATE SCHEMA emall
go

CREATE TABLE emall.Users (
  ID int identity(1,1) primary key  NOT NULL,
  Username varchar(50) NOT NULL,
  [Password] varchar(100) NOT NULL,
  Email varchar(100) NOT NULL,
  FirstName nvarchar(50) DEFAULT NULL,
  LastName nvarchar(50) DEFAULT NULL,
  MiddleName nvarchar(10) DEFAULT NULL,
  RegisterTime datetime NOT NULL DEFAULT GETDATE(),
  OrderCount int  NOT NULL DEFAULT 0,
  Score float NOT NULL DEFAULT 0,
  [Address] nvarchar(50) DEFAULT NULL,
  City nvarchar(50) DEFAULT NULL,
  Province nvarchar(20) DEFAULT NULL,
  Country nvarchar(20) DEFAULT NULL,
  PostalCode nvarchar(20) DEFAULT NULL,
  [Description] nvarchar(200) DEFAULT NULL,
  ShopPage nvarchar(4000) DEFAULT NULL,
  UserType nvarchar(10) NOT NULL,
  PhoneNo nvarchar(20) DEFAULT NULL
) 
go


CREATE TABLE  emall.Products (
  ID int identity(1,1) primary key NOT NULL,
  CategoryID int NOT NULL,
  [Name] nvarchar(50) NOT NULL,
  SellerID int NOT NULL,
  UnitPrice decimal(10,2) NOT NULL,
  Unit nvarchar(10),
  PictureUrl nvarchar(200) DEFAULT NULL,
  [Description] nvarchar(4000) NOT NULL,
  Inventory decimal(10,2) NOT NULL,
  Discount decimal(10,2) NOT NULL,
  Rating decimal(2,1) NOT NULL,
  ReviewCount int NOT NULL,
  SalesCount int NOT NULL
)
go

CREATE TABLE  emall.ProductCategories (
  ID int identity(1,1) primary key NOT NULL,
  ParentID int DEFAULT NULL,
  [Name] nvarchar(100) NOT NULL
) 
go

CREATE TABLE  emall.ProductReviews (
  ID int identity(1,1) primary key NOT NULL,
  ProductID int NOT NULL,
  Review nvarchar(2000) NULL,
  Rating decimal(2,1) NOT NULL,
  CreationTime datetime not null default GETDATE(),
  OrderID int NOT NULL,
  BuyerID int NOT NULL
) 

go

CREATE TABLE  emall.Orders (
  ID int identity(1,1) primary key NOT NULL,
  BuyerID int NOT NULL,
  SellerID int NOT NULL,
  OrderTime datetime NOT NULL DEFAULT GETDATE(),
  TotalBeforeTax decimal(10,2) NOT NULL,
  ShippingBeforeTax decimal(10,2) NOT NULL,
  Taxes decimal(10,2) NOT NULL,
  Total decimal(10,2) NOT NULL,
  OrderStatus  nvarchar (10) NOT NULL,
  ShippingAddress nvarchar(200) NOT NULL,
  Postalcode nvarchar(20) NOT NULL,
  RecieverName nvarchar(100) NOT NULL,
  PhoneNo nvarchar(20) NOT NULL,
  Email nvarchar(100) NOT NULL
) 
go

CREATE TABLE  emall.OrderItems (
  ID int identity(1,1) primary key  NOT NULL,
  OrderID int NOT NULL,
  ProductID int NOT NULL,
  UnitPrice decimal(10,2) NOT NULL,
  BuyPrice decimal(10,2) NOT NULL,
  BuyCount decimal(10,2) NOT NULL,
  Subtotal decimal(10,2) NOT NULL
)
go

CREATE TABLE  emall.CartItems (
  ID int identity(1,1) primary key  NOT NULL,
  SessionID nvarchar(50) NULL,
  BuyerID int NULL,
  SellerID int NULL,
  ProductID int NOT NULL,
  Quantity decimal(10,2) NOT NULL,
  CreationTS datetime NOT NULL DEFAULT GETDATE(),
  [Status] tinyint NOT NULL DEFAULT 0
)
go

ALTER TABLE  emall.Users
	add constraint [uq_users_username] unique(Username),
	constraint [uq_users_email] unique(Email)
go

alter table  emall.Products
	add constraint [fk_products_users] foreign key(SellerID) references  emall.Users(ID),
	constraint [fk_products_productcategories] foreign key(CategoryID) references  emall.ProductCategories(ID)
go

alter table  emall.ProductCategories 
	add constraint [fk_productcategories_productcategories] foreign key(ParentID) references  emall.ProductCategories(ID)
go

alter table  emall.ProductReviews
	add constraint [fk_productreviews_products] foreign key(ProductID) references  emall.Products(ID),
	constraint [fk_productreviews_orders] foreign key(OrderID) references  emall.Orders(ID),
	constraint [fk_productreviews_users] foreign key(BuyerID) references  emall.Users(ID)
go

alter table  emall.Orders
	add constraint [fk_orders_users_buyer] foreign key(BuyerID) references  emall.Users(ID),
	constraint [fk_orders_users_seller] foreign key(SellerID) references  emall.Users(ID)
go

alter table  emall.OrderItems
	add constraint [fk_orderitems_orders] foreign key(OrderID) references  emall.Orders(ID),
		constraint [fk_orderitems_products] foreign key(ProductID) references  emall.Products(ID)
go

alter table  emall.CartItems
	add constraint [fk_cartitems_users_buyer] foreign key(BuyerID) references  emall.Users(ID),
		constraint [fk_cartitems_users_seller] foreign key (SellerID) references  emall.Users(ID),
		constraint [fk_cartitems_products] foreign key(ProductID) references  emall.Products(ID)
go
	







